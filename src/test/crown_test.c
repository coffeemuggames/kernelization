#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <omp.h>

#include "../graph/graph.h"
#include "../graph/graph_builder.h"
#include "../graph/graph_functions.h"
#include "../matching/matching.h"
#include "../matching/matching_functions.h"
#include "../matching/matching_functions_parallel.h"
#include "../crown/crown_functions.h"
#include "../vertex_list/vertex_list.h"
#include "crown_test.h"

extern int additional_graph_info;

int ***graph;
int ***matching0;
int ***matching1;
int ***bipartition;

extern unsigned int *crown_mask;
int *crown; // Were supposed to be external from crown_functions, but is currently deactivated.
int *head;

int main() {

  // Initialize randomness.
  time_t t;
  time(&t);
  srand((unsigned) time(&t));

  test_crown();

  return 0;

}

void test_crown() {

  int k = 83;
  graph = read_graph(fopen("../../graphs/pace2016-1000s/RKT_300_75_30_0.gr", "r"));
  //activate_debug_mode(graph);
  matching0 = create_graph(get_graph_length(graph));
	matching1 = create_graph(get_graph_length(graph));
	bipartition = create_graph(get_graph_length(graph));
  int ***head_crown;

  if (count_vertices(graph) > 3 * k) {

    compute_maximal_matching(graph, matching0);

    if (count_undirected_edges(matching0) <= k) {

			prepare_crown_decomposition(graph, bipartition, matching0, matching1);

      if (count_undirected_edges(matching1) <= k) {

    		printf("crown_value = %i\n", invoke_crown_removal(graph, bipartition, matching1));
        //remove_isolated_vertices(graph);

        int IS = 1;
        int SEP = 1;
        int VERTICES_DOUBLED = 0;
        printf("crown consists of:\n");
        for (int i = 0; i < get_graph_length(graph); i++) {
          if (get_vertex_from_list_by_index(crown, i) > -1) {
            printf("%i\n", get_vertex_from_list_by_index(crown, i) + 1);

            for (int j = 0; j < get_graph_length(graph); j++) {
              if (get_vertex_from_list_by_index(crown, j) > -1
                && is_undirected_edge(graph, get_vertex_from_list_by_index(crown, i), get_vertex_from_list_by_index(crown, j))) {
                IS = 0;
              }
              if (is_undirected_edge(graph, get_vertex_from_list_by_index(crown, i), j)
                && !is_in_list(crown, j) && !is_in_list(head, j)) {
                SEP = 0;
                printf("incorrect edge from %i to %i\n", get_vertex_from_list_by_index(crown, i), j);
              }
            }

          }
        }

        printf("head consists of:\n");
        for (int i = 0; i < get_graph_length(graph); i++) {
          if (get_vertex_from_list_by_index(head, i) > -1) {
            printf("%i\n", get_vertex_from_list_by_index(head, i) + 1);
          }
        }

        for (int i = 0; i < get_graph_length(graph); i++) {
          if (get_vertex_from_list_by_index(crown, i) > -1 && is_in_list(head, get_vertex_from_list_by_index(crown, i))) {
            VERTICES_DOUBLED = 1;
            printf("vertex %i is in both head and crown\n", get_vertex_from_list_by_index(crown, i));
          }
        }

        if (IS) {
          printf("(correct) crown is correct IS\n");
        }
        else {
          printf("(error  ) crown is  not an IS\n");
        }
        if (SEP) {
          printf("(correct) crown has no edges into rest\n");
        }
        else {
          printf("(error  ) crown has edges into rest \n");
        }
        if (VERTICES_DOUBLED) {
          printf("(error  ) vertices are in crown /cut head\n");
        }
        else {
          printf("(correct) crown /cut head is empty\n");
        }

    	}

		}

    free_graph(matching0);
    free_graph(matching1);
    free_graph(bipartition);

  }

  free_graph(graph);

}
