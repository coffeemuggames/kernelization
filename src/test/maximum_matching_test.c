#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <omp.h>

#include "../graph/graph.h"
#include "../graph/graph_builder.h"
#include "../graph/graph_functions.h"
#include "../matching/matching.h"
#include "../matching/matching_functions.h"
#include "../matching/matching_functions_parallel.h"
#include "../vertex_list/vertex_list.h"
#include "../queue/queue.h"
#include "../utilities/time_utilities.h"
#include "maximum_matching_test.h"

extern int parameter;
extern int additional_graph_info;

int ***graph;
int ***matching0;
int ***matching1;
int ***matching_parallel;
int ***matching_nonparallel;
int ***matching_nonparallel_hk;
int ***matching_nonparallel_pr;
int ***bipartition;

int main(int argc, char **argv) {

  // Initialize randomness.
  time_t t;
  time(&t);
  srand((unsigned) time(&t));

  diverge_matchings_huge_graphs(argv[1]);

  return 0;

}

void test_openmp() {

  omp_set_num_threads(omp_get_num_procs());

}

void evaluate_stdin(){

  graph = read_graph(stdin);

  for (int i = 0; i < get_graph_length(graph); i++) {

    if (i < get_graph_length(graph) / 2) set_position_flag(graph, i, 1);
    else set_position_flag(graph, i, 2);

  }

  matching_nonparallel = create_graph(get_graph_length(graph));
  matching_parallel = create_graph(get_graph_length(graph));

  push_relabel_parallel_preinit(graph, matching_parallel);
  compute_maximum_matching_in_bipartition(graph, matching_nonparallel);

  printf("parallel:\n");
  output_graph_uncompactified(matching_parallel);
  printf("nonparallel:\n");
  output_graph_uncompactified(matching_nonparallel);

  free_graph(matching_nonparallel);
  free_graph(matching_parallel);

}

void diverge_matchings_huge_graphs(char *file) {

  int size_parallel = 0, size_nonparallel = 0;

  double time_elapsed = 0;
  struct timespec computation_time_start, computation_time_end;
  int *queue;
  int cnt = 0;

  free(graph);
  free(bipartition);
  free(matching0);
  free(matching1);
  free(matching_parallel);
  free(matching_nonparallel_hk);
  free(matching_nonparallel_pr);

  //graph = read_graph(fopen(file, "r"));
  graph = read_graph(stdin);

  matching0 = create_graph(get_graph_length(graph));
  matching1 = create_graph(get_graph_length(graph));
  bipartition = create_graph(get_graph_length(graph) * 2);

  /*

  start_timer(&computation_time_start, &computation_time_end);
  compute_maximal_matching(graph, matching0);
  time_elapsed = stop_timer(&computation_time_start, &computation_time_end);
  printf("maximal matching nonparallel for bipartite graph: size %i, time %f\n\n", count_undirected_edges(matching0), time_elapsed);

  start_timer(&computation_time_start, &computation_time_end);
  compute_maximal_matching_parallel(graph, matching1);
  time_elapsed = stop_timer(&computation_time_start, &computation_time_end);
  printf("maximal matching parallel for bipartite graph: size %i, time %f\n\n", count_undirected_edges(matching1), time_elapsed);
  */

  duplicate_graph_into_bipartition(graph, bipartition);

  fprintf(stderr,"graph duplicated\n");

  matching_nonparallel_hk = create_graph(get_graph_length(bipartition));
  matching_nonparallel_pr = create_graph(get_graph_length(bipartition));
  matching_parallel = create_graph(get_graph_length(bipartition));

  // HOPCROFT-KARP
  /*
  start_timer(&computation_time_start, &computation_time_end);
  compute_maximal_matching(bipartition, matching_nonparallel_hk);
  compute_maximum_matching_in_bipartition(bipartition, matching_nonparallel_hk);
  time_elapsed = stop_timer(&computation_time_start, &computation_time_end);
  printf("hopcroft-karp: size (%i), time %f\n", count_undirected_edges(matching_nonparallel_hk), time_elapsed);

  cnt = 0;
  start_timer(&computation_time_start, &computation_time_end);
  queue = compute_min_vc_from_bipartition_and_matching(bipartition, matching_nonparallel_hk);
  time_elapsed = stop_timer(&computation_time_start, &computation_time_end);
  while (pop_queue(queue) != -1) cnt++;
  printf("min-vc from hopcroft-karp: size %i, time %f\n\n", cnt, time_elapsed);
  */

  // PUSH-RELABEL NONPARALLEL

  start_timer(&computation_time_start, &computation_time_end);
  push_relabel_nonparallel(bipartition, matching_nonparallel_pr);
  time_elapsed = stop_timer(&computation_time_start, &computation_time_end);
  printf("push relabel nonparallel: size (%i), time %f\n", count_undirected_edges(matching_nonparallel_pr), time_elapsed);

  cnt = 0;
  start_timer(&computation_time_start, &computation_time_end);
  queue = compute_min_vc_from_bipartition_and_matching(bipartition, matching_nonparallel_pr);
  time_elapsed = stop_timer(&computation_time_start, &computation_time_end);
  while (pop_queue(queue) != -1) cnt++;
  printf("min-vc from push relabel nonparallel: size %i, time %f\n\n", cnt, time_elapsed);

  // PUSH-RELABEL PARALLEL

  start_timer(&computation_time_start, &computation_time_end);
  compute_maximal_matching(bipartition, matching_parallel);
  push_relabel_parallel_preinit(bipartition, matching_parallel);
  time_elapsed = stop_timer(&computation_time_start, &computation_time_end);
  printf("push-relabel parallel: size (%i), time %f\n", count_undirected_edges(matching_parallel), time_elapsed);

  cnt = 0;
  start_timer(&computation_time_start, &computation_time_end);
  queue = compute_min_vc_from_bipartition_and_matching(bipartition, matching_parallel);
  time_elapsed = stop_timer(&computation_time_start, &computation_time_end);
  while (pop_queue(queue) != -1) cnt++;
  printf("min-vc from push-relabel parallel: size %i, time %f\n", cnt, time_elapsed);

  free(queue);

  free_graph(graph);
  free_graph(matching0);
  free_graph(bipartition);
  free_graph(matching_nonparallel_hk);
  free_graph(matching_nonparallel_pr);
  free_graph(matching_parallel);

}

void diverge_matchings() {

  int size_parallel = 0, size_nonparallel = 0;

  while (size_parallel == size_nonparallel) {

    printf("|");

    graph = create_graph(rand() % 50 + 10);
    bipartition = create_graph(get_graph_length(graph) * 2);
    matching_nonparallel = create_graph(get_graph_length(bipartition));
    matching_parallel = create_graph(get_graph_length(bipartition));

    // Create random graph.
    for (int i = 0; i < get_graph_length(graph); i++) {

      for (int j = i + 1; j < get_graph_length(graph); j++) {

        if (rand() % 2 == 0) {

          add_undirected_edge(graph, i, j);

        }

      }

    }

    duplicate_graph_into_bipartition(graph, bipartition);

    push_relabel_parallel_preinit(bipartition, matching_parallel);
    size_parallel = count_undirected_edges(matching_parallel);
    //printf("%i = ", size_parallel);

    compute_maximum_matching_in_bipartition(bipartition, matching_nonparallel);
    size_nonparallel = count_undirected_edges(matching_nonparallel);
    //printf("%i ", size_nonparallel);

  }


  printf("\nfound it\n");
  printf("   graph\n");
  output_graph_uncompactified(bipartition);
  printf("nonparallel\n");
  output_graph_uncompactified(matching_nonparallel);
  printf("parallel\n");
  output_graph_uncompactified(matching_parallel);

  free_graph(graph);
  free_graph(bipartition);
  free_graph(matching_nonparallel);
  free_graph(matching_parallel);

}

/*
  duplicate_graph_into_bipartition
  Duplicates the vertex set of a graph (vertex u into u1, u2) and for every edge
  u-v adds two edges u1-v2, v1-u2, resulting in a bipartite graph.
  Params: graph - graph
          bipartition - an empty graph, result will be stored here
*/
void duplicate_graph_into_bipartition(int ***graph, int ***bipartition) {

  int *vertex_degree = (int *)(calloc(get_graph_length(bipartition), sizeof(int)));
	int *edge_list_index = (int *)(calloc(get_graph_length(bipartition), sizeof(int)));

  int half_size = get_graph_length(graph);
  int other = -1;

  for (int this = 0; this < half_size; this++) {

    for (int j = additional_graph_info; j < graph[this][0][0]; j++) {

      other = graph[this][0][j];

      // Add every edge only once.
      if (other > this) { // This way, this > -1 and every edge is only added once.

        set_deleted_flag(bipartition, this, get_deleted_flag(graph, this));
        set_deleted_flag(bipartition, this + half_size, get_deleted_flag(graph, this));
        set_deleted_flag(bipartition, other, get_deleted_flag(graph, other));
        set_deleted_flag(bipartition, other + half_size, get_deleted_flag(graph, other));

        set_position_flag(bipartition, this, 1);
        set_position_flag(bipartition, other, 1);
        set_position_flag(bipartition, other + half_size, 2);
        set_position_flag(bipartition, this + half_size, 2);

        //add_undirected_edge(bipartition, this, other + half_size);
        //add_undirected_edge(bipartition, other, this + half_size);

        add_undirected_edge_fast(bipartition, vertex_degree, edge_list_index, this, other + half_size);
        add_undirected_edge_fast(bipartition, vertex_degree, edge_list_index, other, this + half_size);

      }

    }

  }

  free(vertex_degree);
  free(edge_list_index);

}
