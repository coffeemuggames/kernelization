void duplicate_graph_into_bipartition(int ***, int ***);
void test_openmp();
void evaluate_stdin();
void diverge_matchings();
void diverge_matchings_huge_graphs(char *);
