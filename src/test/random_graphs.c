#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <omp.h>

#include "../graph/graph.h"
#include "../graph/graph_builder.h"
#include "../graph/graph_functions.h"
#include "../matching/matching.h"
#include "../matching/matching_functions.h"
#include "../matching/matching_functions_parallel.h"
#include "random_graphs.h"

extern int additional_graph_info;
extern int parameter;
extern double computation_time;

int main(int argc, char **argv) {

  // Initialize randomness.
  time_t t;
  time(&t);
  srand((unsigned) time(&t));

  create_random_graph_unclustered(strtol(argv[1], NULL, 10), strtol(argv[2], NULL, 10));

  return 0;

}

/*
  create_random_graph_unclustered
  Creates a random graph without clustered vertices with a lot of edges.
  Params:   n - the number of vertices
            k - the size of a minimum vertex cover in the resulting graph
*/
void create_random_graph_unclustered(int n, int k) {

  int *edge_count = (int *)(calloc(n, sizeof(int)));
  int **edges = (int **)(malloc(k * sizeof(int *)));
  int r = -1, first = -1;
  int m = 0;

  fprintf(stderr, "Step 1/4\n");

  for (int i = 0; i < k; i++) {

    edges[i] = (int *)(malloc(n * sizeof(int)));
    output_progress((int)(i/(double)k * 100));

  }

  fprintf(stderr,"\nStep 2/4\n");

  for (int i = k; i < n; i++) {

    output_progress((int)(i/(double)n * 100));

    first = rand() % k;
    edges[first][edge_count[first]++] = i;
    m++;

    for (int j = 0; j < k; j++) {

      if (j != first && rand() % 150 == 1) {

        edges[j][edge_count[j]++] = i;
        m++;

      }

    }

  }

  fprintf(stderr, "\nStep 3/4\n");

  for (int i = 0; i < k; i++) {

    output_progress((int)(i/(double)k * 100));

    for (int j = i + 1; j < k; j++) {

      if (rand() % 3 == 1) {

        edges[j][edge_count[j]++] = i;
        m++;

      }

    }

  }

  fprintf(stderr, "\nStep 4/4\n");

  printf("p tw %i %i\n", n, m);

  for (int i = 0; i < k; i++) {

    output_progress((int)(i/(double)k * 100));

    for (int j = 0; j < edge_count[i]; j++) {

      printf("%i %i\n", i + 1, edges[i][j] + 1);

    }

  }

}

/*
  output_progress
  Outputs the current progress.
  Params:   percentage - the current progress
*/
void output_progress(int percentage) {

  if (percentage == 99) percentage = 100;
  fprintf(stderr, "\r%i%%", percentage);
  fflush(stderr);

}

/*
  create_random_graph
  Creates a random graph with clustered vertices with a lot of edges.
  Params:   n - the number of vertices
            k - the size of a minimum vertex cover in the resulting graph
*/
void create_random_graph(int n, int k) {

  int *edge_list_position = (int *)(calloc(n, sizeof(int)));
  int edge_count = 0;
  int g_length = 2 * n;
  int *g = (int *)(calloc(g_length, sizeof(int)));
  int **edges = (int **)(malloc(k * sizeof(int *)));
  int *edge_counters = (int *)(calloc(k, sizeof(int)));

  int *names = (int *)(malloc(n * sizeof(n)));
  int temp = -1, other = -1;

  for (int i = 0; i < k; i++) {

    edges[i] = (int *)(calloc(n, sizeof(int)));

  }

  for (int i = 0; i < n; i++) names[i] = i + 1;

  for (int i = 0; i < n; i++) {

    other = rand() % n;
    temp = names[other];
    names[other] = names[i];
    names[i] = temp;

  }

  for (int i = k; i < n; i++) {

    if (i < 2 * k) {

      g[2 * edge_count] = i - k + 1;
      g[2 * edge_count + 1] = i + 1;
      edge_count++;
      if (2 * edge_count > g_length) enlarge_array(&g, 2 * edge_count, 100);

    }
    else {

      g[2 * edge_count] = rand() % k + 1;
      g[2 * edge_count + 1] = i + 1;
      edge_count++;
      if (2 * edge_count > g_length) enlarge_array(&g, 2 * edge_count, 100);

    }

  }

  for (int i = 0; i < k; i++) {

    for (int j = i + 1; j < k; j++) {

      if (rand() % 3 == 0) {

        g[2 * edge_count] = i + 1;
        g[2 * edge_count + 1] = j + 1;
        edge_count++;
        if (2 * edge_count > g_length) enlarge_array(&g, 2 * edge_count, 100);

      }

    }

  }

  printf("p tw %i %i\n", n, edge_count);

  for (int i = 0; i < edge_count; i++) {

    edges[g[2 * i] - 1][edge_counters[g[2 * i] - 1]] = g[2 * i + 1];
    edge_counters[g[2 * i] - 1]++;

  }

  for (int i = 0; i < k; i++) {

    for (int j = 0; j < n; j++) {

      if (edges[i][j] > 0) {

        printf("%i %i\n", names[i], names[edges[i][j] - 1]);

      }

    }

  }

}


/*
  enlarge_array
  Enlarges an array.
  Params:   array - a pointer to a pointer to an array, the array will be set
            current_size - the current size of array
            enlargement - the amount by which the array will be enlarged
*/
void enlarge_array(int **array,int current_size, int enlargement) {

  *array = (int *)(realloc(*array, (current_size + enlargement) * sizeof(int)));

}
