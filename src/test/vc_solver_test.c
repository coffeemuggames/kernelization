#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <omp.h>

#include "../graph/graph.h"
#include "../graph/graph_builder.h"
#include "../graph/graph_functions.h"
#include "../vertex_list/vertex_list.h"
#include "../queue/queue.h"
#include "../vc_solver/vc_functions.h"
#include "vc_solver_test.h"

extern int parameter;
extern int additional_graph_info;

int ***graph;

int main() {

  // Initialize randomness.
  time_t t;
  time(&t);
  srand((unsigned) time(&t));

  pointer_test();
  //solve_k_graphs();

  return 0;

}

void pointer_test() {

  int *p = (int *)(calloc(10, sizeof(int)));
  replace_pointer_param(&p);

  for (int i = 0; i < 10; i++) {

    printf("%i ", p[i]);

  }

  free(p);

  printf("\n");

}

void replace_pointer_param(int **old) {

  int *new = (int *)(malloc(10 * sizeof(int)));

  for (int i = 0; i < 10; i++) {

    new[i] = i+1;

  }

  *old = new;

}

void test_connected_components() {

  int ***graph = create_graph(20);
  int *deleted_vertices = (int *)(calloc(get_graph_length(graph), sizeof(int)));
  int *start_of_component = (int *)(malloc(get_graph_length(graph) * sizeof(int)));
  int *predecessor = (int *)(malloc(get_graph_length(graph) * sizeof(int)));
  int *successor = (int *)(malloc(get_graph_length(graph) * sizeof(int)));

  for (int i = 0; i < get_graph_length(graph) - 1; i++) {

    if (i % 10 != 0 || i == 0) {

      add_undirected_edge(graph, i, i + 1);

    }
    else {

      add_undirected_edge(graph, i, i - 10);

    }

  }

  output_graph(graph);

  int *label = determine_connected_components_dfs(graph, deleted_vertices, start_of_component, predecessor, successor);

  for (int i = 0; i < get_graph_length(graph); i++) {

    printf("vertex %i is in component %i\n", i+1, label[i]);

  }

  free(label);
  free_graph(graph);
  free(deleted_vertices);

}

void solve_k_graphs() {

  int n = 1000;
  int *initial_list = (int *)(malloc(n * sizeof(int)));

  for (int k = 2; k <= n / 2 - 1; k++) {

    printf("k = %i\n", k );

    graph = create_k_graph(n, k);
    parameter = k;

    for (int i = 0; i < n; i++) initial_list[i] = 0;

    printf("vertex cover of %i found, %i was asked \n", start_vc_computation(graph, parameter, count_undirected_edges(graph), &initial_list),
      k);

    free_graph(graph);

  }

  free(initial_list);

}

int ***create_k_graph(int n, int k) {

  int ***k_graph = create_graph(n);
  int edge_count = 0;
  int neighbor = -1;

  for (int i = 0; i < k; i++) {

    edge_count = (rand() % (n-k-1)) + 1;

    for (int j = k; j < n; j++) {

      //neighbor = k + (((rand() % 3 + 1) * j) % (n - k));
      neighbor = j;

      add_undirected_edge(k_graph, i, neighbor);

    }

  }

  return k_graph;

}
