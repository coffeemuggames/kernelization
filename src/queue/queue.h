int * create_queue(int);
void push_queue(int *, int);
int pop_queue(int *);

int get_queue_start_position(int *);
int get_queue_end_position(int *);
int get_queue_entries_start(int *);
int get_queue_length(int *);
