#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "queue.h"

int additional_queue_info = 3; // length of the queue, start and end are saved

/*
	create_queue
	Creates a queue of a specific size.
	Params:		size - the maximum number of elements that will be in the queue
							at a certain point of time
	Returns:	queue - the queue
*/
int * create_queue(int size) {

	int *queue = (int *)(malloc((size	+ additional_queue_info) * sizeof(int)));

	for (int i = 0; i < size + additional_queue_info; i++) {

		queue[i] = -1;

	}

	queue[0] = size;
	queue[1] = additional_queue_info;
	queue[2] = additional_queue_info - 1;

	return queue;

}

/*
	push_queue
	Pushes an element onto a queue at the end of that queue.
	Params:		queue - a queue
						a - an element
*/
void push_queue(int *queue, int a) {

	queue[2]++;

	if (queue[2] >= queue[0] + additional_queue_info) {

		queue[2] = additional_queue_info;

	}

	queue[queue[2]] = a;

}

/*
	pop_queue
	Returns the first element from a queue and deletes it.
	Params:		queue - a queue
	Returns:	ret - the first element of the queue, -1 if empty
*/
int pop_queue(int *queue) {

	int ret = queue[queue[1]];
	queue[queue[1]] = -1;
	queue[1]++;

	if (queue[1] >= queue[0] + additional_queue_info) {

		queue[1] = additional_queue_info;

	}

	// If queue is empty, reset pointers.
	if (ret == -1) {

		queue[1] = additional_queue_info;
		queue[2] = additional_queue_info - 1;

	}

	return ret;

}

/*
	get_start_position
	Params:		queue - a queue
	Returns: 	start - the index of the first entry in the queue (may be -1)
*/
int get_queue_start_position(int *queue) {

	return queue[1];

}

/*
	get_end_position
	Params:		queue - a queueu
	Returns:	end - the index of the last entry in the queue (may be -1)
*/
int get_queue_end_position(int *queue) {

	return queue[2];

}

/*
	get_entries_start
	Params:		queue - a queue
	Returns:	entries_start - the index in the queue where the first item can be held
							memorywise. Current entries may be at different positions.
*/
int get_queue_entries_start(int *queue) {

	return additional_queue_info;

}

/*
	get_length
	Params:		queue - a queue
	Returns:	length - the length of queue in memory
*/
int get_queue_length(int *queue) {

	return queue[0] + additional_queue_info;

}
