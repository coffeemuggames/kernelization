void start_timer(struct timespec *, struct timespec *);
double stop_timer(struct timespec *, struct timespec *);
void output_time_elapsed(struct timespec *, struct timespec *, double);
void start_cpu_timer(clock_t);
double stop_cpu_timer(clock_t);
