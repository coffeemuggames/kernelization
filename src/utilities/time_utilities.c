#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "time_utilities.h"

double time_elapsed;
clock_t end;

/*
  start_timer
  Starts a timer to measure time.
  Params:   start - a timespec struct to save start time to
            end - a timespec struct to save end time to
*/
void start_timer(struct timespec *start, struct timespec *end) {

  clock_gettime(CLOCK_REALTIME, start);

}

/*
  stop_timer
  Stops a timer to measure time.
  Params:   start - a timespec struct that has probably been used with start_timer
            end - a timespec struct to save end time to
  Returns:  time_elapsed - the time that has elapsed between start and end
              in seconds
*/
double stop_timer(struct timespec *start, struct timespec *end) {

  clock_gettime(CLOCK_REALTIME, end);

  time_elapsed = (*end).tv_sec - (*start).tv_sec;
  time_elapsed += ((*end).tv_nsec - (*start).tv_nsec)  / (double)1000000000;

  return time_elapsed;

}

/*
  output_time_elapsed
  Stops a timer to measure time, then outputs the time in graph format.
  Params:   start - a timespec struct that has probably been used with start_timer
            end - a timespec struct to save end time to
*/
void output_time_elapsed(struct timespec *start, struct timespec *end, double preelapsed_computation_time) {

  time_elapsed = (*end).tv_sec - (*start).tv_sec;
  time_elapsed += ((*end).tv_nsec - (*start).tv_nsec)  / (double)1000000000;

  printf("c time computation %f\n", preelapsed_computation_time + time_elapsed);

}

/*
  start_cpu_timer
  Starts a timer for measuring used cpu time.
  Params:   timer - a timer
*/
void start_cpu_timer(clock_t timer) {

  timer = clock();

}

/*
  stop_cpu_timer
  Stops the timer for measuring used time cpu.
  Params:   timer - the timer
  Returns:  diff - the difference between start and end of the timer
*/
double stop_cpu_timer(clock_t timer) {

  end = clock();
  return ((double)(end - timer)) / CLOCKS_PER_SEC;

}
