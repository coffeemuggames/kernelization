int start_vc_computation(int ***, int, int, int **);
int compute_vc_initialized(int ***, int, int, int **, int);

int degree_with_deleted_vertices(int ***, int , int *);
int find_max_degree_vertex(int ***, int *);

int * clone_deleted_vertices(int ***, int *);
int update_deleted_vertices_with_deletion(int ***, int , int *);
int update_deleted_vertices_with_neighborhood_deletion(int ***, int , int *);

int solve_vc_max_degree_two(int ***, int *);
int * determine_connected_components_dfs(int ***, int *, int *, int *, int *);
void labeling_dfs(int ***, int, int, int *, int *, int *, int *, int *);
void connect_vertices(int, int, int *, int *);
