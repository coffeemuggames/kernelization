#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <omp.h>
#include <math.h>

#include "../graph/graph.h"
#include "../graph/graph_builder.h"
#include "../graph/graph_functions.h"
#include "../matching/matching.h"
#include "../matching/matching_functions.h"
#include "../matching/matching_functions_parallel.h"
#include "../vertex_list/vertex_list.h"
#include "../queue/queue.h"
#include "../utilities/time_utilities.h"
#include "vc_functions.h"
#include "min_vc.h"

// Allows for usage of only a percentage of the cpus (use multiples of 2).
#define CPU_USAGE_FACTOR 1

extern int parameter;
extern int additional_graph_info;

int ***graph;
int *initial_list;
int vc_size;

int main(int argc, char **argv) {

  # ifdef _OPENMP
		omp_set_num_threads(omp_get_num_procs() / CPU_USAGE_FACTOR);
	# endif

  parameter = -1;
  vc_size = -1;

  graph = read_graph(stdin);
  initial_list = (int *)(calloc(get_graph_length(graph), sizeof(int)));

  vc_size = find_min_vc();
  output_vc(graph, initial_list, vc_size);

  free_graph(graph);

  return 1;

}

/*
  find_min_vc
  Determines a minimum sized vertex cover.
*/
int find_min_vc() {

  int ret = -1;
  int l = 0, r = get_graph_length(graph), mid = -1;

  while (l <= r) {

    mid = l + (r - l) / 2;

    ret = start_vc_computation(graph, mid, count_undirected_edges(graph), &initial_list);

    if (ret > mid || ret == -1) { // NO

      l = mid + 1;

    }
    else { // YES

      r = mid - 1;

    }

    if (l <= r) clear_vc_list();

  }

  return ret;

}

/*
  clear_vc_list
  Sets all entries in the list for storing a vertex cover to zero.
*/
void clear_vc_list() {

  for (int i = 0; i < get_graph_length(graph); i++) {

    initial_list[i] = 0;

  }

}
