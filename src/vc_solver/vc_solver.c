#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <omp.h>
#include <math.h>

#include "../graph/graph.h"
#include "../graph/graph_builder.h"
#include "../graph/graph_functions.h"
#include "../matching/matching.h"
#include "../matching/matching_functions.h"
#include "../matching/matching_functions_parallel.h"
#include "../vertex_list/vertex_list.h"
#include "../queue/queue.h"
#include "../utilities/time_utilities.h"
#include "vc_functions.h"
#include "vc_solver.h"

// Allows for usage of only a percentage of the cpus (use multiples of 2).
#define CPU_USAGE_FACTOR 1

extern int parameter;
extern int additional_graph_info;

int ***graph;
int *initial_list;
int vc_size;

int main(int argc, char **argv) {

  # ifdef _OPENMP
		omp_set_num_threads(omp_get_num_procs() / CPU_USAGE_FACTOR);
	# endif

  parameter = -1;
  graph = read_graph(stdin);

  if (parameter == -1) {

    parameter = get_graph_length(graph) - 1;

  }

  if (parameter >= get_graph_length(graph)) {

    output_full_graph_vc(graph);

  }
  else {

    handle_nontrivial_vc_solving();

  }

  free_graph(graph);

  return 1;

}

/*
  handle_nontrivial_vc_solving
  If the input graph is nontrivial, this functions solves the vertex cover
    problem.
*/
void handle_nontrivial_vc_solving() {

  initial_list = (int *)(calloc(get_graph_length(graph), sizeof(int)));
  vc_size = -1;

  vc_size = start_vc_computation(graph, parameter, count_undirected_edges(graph), &initial_list);

  if (vc_size <= parameter) {

    output_vc(graph, initial_list, vc_size);

  }
  else {

    output_no_vc(graph);

  }

  free(initial_list);

}
