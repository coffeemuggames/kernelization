#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <omp.h>
#include <math.h>

#include "../graph/graph.h"
#include "../graph/graph_builder.h"
#include "../graph/graph_functions.h"
#include "../matching/matching.h"
#include "../matching/matching_functions.h"
#include "../matching/matching_functions_parallel.h"
#include "../vertex_list/vertex_list.h"
#include "../queue/queue.h"
#include "../utilities/time_utilities.h"
#include "vc_functions.h"

extern int parameter;
extern int additional_graph_info;

int vc_found = 0;

struct timespec io_time_start, io_time_end;

/*
  start_vc_computation
  Starts the computation of a minimum vertex cover.
  Params:   graph - a graph
            k - the maximum size of a vertex cover
            m - the the number of edges in graph
            deleted_vertices - a list with a flag determining deletion of vertices
              in addition to deleted flag of graph
  Returns:  vc_size - The size of a minimum vertex cover that was found
*/
int start_vc_computation(int ***graph, int k, int m, int **deleted_vertices) {

  vc_found = 0;
  return compute_vc_initialized(graph, k, m, deleted_vertices, 0);

}

/*
  compute_vc_initialized
  Computes a minimum vertex cover. Will be called by start_vc_computation.
  Params:   graph - a graph
            k - the maximum size of a vertex cover
            m - the the number of edges in graph
            deleted_vertices - a list with a flag determining deletion of vertices
              in addition to deleted flag of graph
  Returns:  vc_size - The size of a minimum vertex cover that was found

*/
int compute_vc_initialized(int ***graph, int k, int m, int **deleted_vertices, int vc_size) {

  int ret = get_graph_length(graph);
  int vc_size_deg2 = -1;
  int v = -1;
  int deg = 0;
  int m1 = -1, m2 = -1;
  int r1, r2;
  int *l1, *l2;

  if (k >= 0 && !vc_found) {

    v = find_max_degree_vertex(graph, *deleted_vertices);

    if (k >= m / degree(graph, v)) {

      if (m == 0) {

        ret = vc_size;
        vc_found = 1;

      }
      else {

        if (degree_with_deleted_vertices(graph, v, *deleted_vertices) > 2) {

          l1 = clone_deleted_vertices(graph, *deleted_vertices);
          l2 = clone_deleted_vertices(graph, *deleted_vertices);

          deg = degree_with_deleted_vertices(graph, v, *deleted_vertices);
          m1 = m - update_deleted_vertices_with_deletion(graph, v, l1);
          m2 = m - update_deleted_vertices_with_neighborhood_deletion(graph, v, l2);

          #pragma omp parallel
          #pragma omp single nowait
          {

            #pragma omp task shared(r1)
            {

              r1 = compute_vc_initialized(graph, k - 1, m1, &l1, vc_size + 1);

            }

            #pragma omp task shared(r2)
            {

              r2 = compute_vc_initialized(graph, k - deg, m2, &l2, vc_size + deg);

            }

            #pragma omp taskwait
            {

              free(*deleted_vertices);
              *deleted_vertices = NULL;

              if (r1 < r2) {

                ret = r1;
                *deleted_vertices = l1;
                free(l2);
                l2 = NULL;

              }
              else {

                ret = r2;
                *deleted_vertices = l2;
                free(l1);
                l1 = NULL;

              }

            }

          }

        }
        else {

          vc_size_deg2 = solve_vc_max_degree_two(graph, *deleted_vertices);
          ret = vc_size + vc_size_deg2;
          if (vc_size_deg2 <= k) {

            vc_found = 1;

          }


        }

      }

    }

  }

  return ret;

}

/*
  solve_vc_max_degree_two
  Computes a minimum vertex cover in a graph with maximum degree of two.
  Params:   graph - a graph
  Returns:  deleted_vertices - a list with a flag determining deletion of vertices
              in addition to deleted flag of graph
*/
int solve_vc_max_degree_two(int ***graph, int *deleted_vertices) {

  int *start_of_component = (int *)(malloc(get_graph_length(graph) * sizeof(int)));
  int *predecessor = (int *)(malloc(get_graph_length(graph) * sizeof(int)));
  int *successor = (int *)(malloc(get_graph_length(graph) * sizeof(int)));
  int *label = determine_connected_components_dfs(graph, deleted_vertices, start_of_component, predecessor, successor);

  int *inserted = (int *)(calloc(get_graph_length(graph), sizeof(int)));
  int *component_size = (int *)(calloc(get_graph_length(graph), sizeof(int)));
  int *is_path = (int *)(calloc(get_graph_length(graph),  sizeof(int)));
  int *queue = create_queue(get_graph_length(graph));
  int elem = -1, ret = 0;

  int traverse = 1, current_vertex = -1, last_vertex = -1, take_into_vc = 0;

  for (int i = 0; i < get_graph_length(graph); i++) {

    if (!get_deleted_flag(graph, i) && !deleted_vertices[i]) {

      component_size[label[i]]++;
      if (!inserted[label[i]] && component_size[label[i]] > 1) {

          push_queue(queue, label[i]);
          inserted[label[i]] = 1;

      }

      if (degree_with_deleted_vertices(graph, i, deleted_vertices) == 1) {

        is_path[label[i]] = 1;

      }

    }

  }

  while((elem = pop_queue(queue)) != -1) {

    current_vertex = start_of_component[elem];
    last_vertex = -1;
    take_into_vc = 1;
    traverse = 1;

    if (is_path[elem]) {
      take_into_vc = 0;
    }

    while(traverse) {

      traverse = 0;

      if (take_into_vc) {
        deleted_vertices[current_vertex] = 1;
      }
      take_into_vc = 1 - take_into_vc;

      if (predecessor[current_vertex] != -1 && predecessor[current_vertex] != last_vertex
        && predecessor[current_vertex] != start_of_component[elem]) {

        last_vertex = current_vertex;
        current_vertex = predecessor[current_vertex];
        traverse = 1;

      }
      else if (successor[current_vertex] != -1 && successor[current_vertex] != last_vertex
        && successor[current_vertex] != start_of_component[elem]) {

        last_vertex = current_vertex;
        current_vertex = successor[current_vertex];
        traverse = 1;

      }

    }

    ret += component_size[elem] / 2;

    if (!is_path[elem] && component_size[elem] % 2 == 1) {

      ret++;

    }

  }

  free(start_of_component);
  free(predecessor);
  free(successor);
  free(label);
  free(inserted);
  free(component_size);
  free(queue);
  free(is_path);

  return ret;

}

/*
  determine_connected_components_dfs
  Determines the connected components in a graph.
  Params:   graph - a graph
            deleted_vertices - a list of deleted vertices, used for solving vertex cover
            start_of_component - a list in which the start vertex of a component will be
              saved for each component (for traversal)
            predecessor - a list which saves the predecessor for each vertex in
              graph in its component
            successor - a list which saves the successor for each vertex in
              graph in its component
  Returns:  label - a list in which the component id is set for each vertex
*/
int * determine_connected_components_dfs(int ***graph, int *deleted_vertices, int *start_of_component,
      int *predecessor, int *successor) {

  int *label = (int *)(malloc(get_graph_length(graph) * sizeof(int)));
  int component = 1;

  for (int i = 0; i < get_graph_length(graph); i++) {

    label[i] = 0;
    predecessor[i] = -1;
    successor[i] = -1;
    start_of_component[i] = -1;

  }

  for (int v = 0; v < get_graph_length(graph); v++) {

    if (!get_deleted_flag(graph, v) && !deleted_vertices[v] && !label[v]) {

      labeling_dfs(graph, v, component, deleted_vertices, label, start_of_component, predecessor, successor);
      component++;

    }

  }

  return label;

}

/*
  labeling_dfs
  Floods a component with a label through a given vertex
  Params:   graph - a graph
            v - a vertex in graph through which graph will be flooded to determine
              the connected component of v
            component - the id which will be used to mark all vertices in the connected
              component of v
            deleted_vertices - a list of deleted vertices, used for solving vertex cover
            label - a list in which the task will be fullfilled
            start_of_component - a list in which the start vertex of a component will be
              saved for each component (for traversal)
            predecessor - a list which saves the predecessor for each vertex in
              graph in its component
            successor - a list which saves the successor for each vertex in
              graph in its component
  Returns:  label - a list in which the component id is set for each vertex
*/
void labeling_dfs(int ***graph, int v, int component, int *deleted_vertices,
      int *label, int *start_of_component, int *predecessor, int *successor) {

  label[v] = component;

  // Set start vertex of component for traversal later on.
  // The start vertex will be reset to another vertex, if currently
  // there is direct neighbor or the vertex itself that has only
  // one connection to another vertex (not two).
  if (predecessor[v] == -1 || successor[v] == -1) {
    start_of_component[label[v]] = v;
  } else {
    if (predecessor[v] != -1) {
      if (predecessor[predecessor[v]] == -1 || successor[predecessor[v]] == -1) {
        start_of_component[label[v]] = predecessor[v];
      }
    }
    if (successor[v] != -1) {
      if (predecessor[successor[v]] == -1 || successor[successor[v]] == -1) {
        start_of_component[label[v]] = successor[v];
      }
    }
  }

  for (int i = additional_graph_info; i < graph[v][0][0]; i++) {

    if (graph[v][0][i] > -1 && !get_deleted_flag(graph, graph[v][0][i]) && !deleted_vertices[graph[v][0][i]]
        && !label[graph[v][0][i]]) {

      connect_vertices(v, graph[v][0][i], predecessor, successor);
      labeling_dfs(graph, graph[v][0][i], component, deleted_vertices, label, start_of_component, predecessor, successor);

    }

  }

}

/*
  connect_vertices
  Connects to vertices for traversal.
  Params:   u - a vertex in graph
            v - a vertex in graph
            predecessor - a list which saves the predecessor for each vertex in
              graph in its component
            successor - a list which saves the successor for each vertex in
              graph in its component
*/
void connect_vertices(int u, int v, int *predecessor, int *successor) {

  if (predecessor[u] == -1) {
    predecessor[u] = v;
  }
  else {
    successor[u] = v;
  }

  if (predecessor[v] == -1) {
    predecessor[v] = u;
  }
  else {
    successor[v] = u;
  }

}

/*
  degree_with_deleted_vertices
  Params:   vertex - a vertex in graph
            deleted_vertices - a list of deleted vertices in graph
  Returns:  the degree of the specified vertex with respect to a list of deleted
              vertices
*/
int degree_with_deleted_vertices(int ***graph, int vertex, int *deleted_vertices) {

	int degree = 0;

	for (int i = additional_graph_info; i < graph[vertex][0][0]; i++) {

		if (graph[vertex][0][i] > -1 && !get_deleted_flag(graph, graph[vertex][0][i]) && !deleted_vertices[graph[vertex][0][i]]) {

			degree++;

		}

	}

	return degree;



}

/*
  find_max_degree_vertex
  Finds a vertex of maximum degree in graph.
  Params:   deleted_vertices - a list annotating for every vertex in graph
              whether or not it is deleted
  Returns:  v - a vertex in graph with maximal degree
*/
int find_max_degree_vertex(int ***graph, int *deleted_vertices) {

  int v = -1;
  int max_deg = -1, cur_deg = 0;

  for (int i = 0; i < get_graph_length(graph); i++) {

    if (!get_deleted_flag(graph, i) && !deleted_vertices[i]) {

      cur_deg = degree_with_deleted_vertices(graph, i, deleted_vertices);

      if (cur_deg > max_deg || max_deg == -1) {

        v = i;
        max_deg = cur_deg;

      }

    }

  }

  return v;

}

/*
  clone_deleted_vertices
  Clones a list of deleted vertices for graph.
  Params:   deleted_vertices - a list annotating deleted vertices in graph
  Returns:  copy - a copy of deleted_vertices
*/
int * clone_deleted_vertices(int ***graph, int *deleted_vertices) {

  int *copy = (int *)(malloc(get_graph_length(graph) * sizeof(int)));

  for (int i = 0; i < get_graph_length(graph); i++) {

    copy[i] = deleted_vertices[i];

  }

  return copy;

}

/*
  update_deleted_vertices_with_deletion
  Removes a vertex from graph, but annotates it only in a list.
  Params:   vertex - a vertex in graph
            deleted_vertices - a list annotating for every vertex in graph
              whether or not it is deleted; this list will be updated
  Returns:  count - the number of edges that were removed through deletion of
              vertex
*/
int update_deleted_vertices_with_deletion(int ***graph, int vertex, int *deleted_vertices) {

  int ret = degree_with_deleted_vertices(graph, vertex, deleted_vertices);
  deleted_vertices[vertex] = 1;
  return ret;

}

/*
  update_deleted_vertices_with_neighborhood_deletion
  Removes all neighbors of a vertex from graph, but annotates it only in a list.
  Params:   vertex - a vertex in graph
            deleted_vertices - a list annotating for every vertex in graph
              whether or not it is deleted; this list will be updated
  Returns:  count - the number of edges that were removed through deletion of
              the neighbors of vertex
*/
int update_deleted_vertices_with_neighborhood_deletion(int ***graph, int vertex, int *deleted_vertices) {

  int count = 0;
  int neighbor = -1;

    for (int i = additional_graph_info; i < graph[vertex][0][0]; i++) {

      neighbor = graph[vertex][0][i];

      if (neighbor > -1 && !get_deleted_flag(graph, neighbor) && !deleted_vertices[neighbor]) {

        count += update_deleted_vertices_with_deletion(graph, neighbor, deleted_vertices);

      }

    }

    return count;

}
