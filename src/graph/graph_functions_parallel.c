#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>
#include "graph_functions_parallel.h"

/*
	write_to_bitmask
	Writes to a bitmask in a parallel way.
	Params:		bitmask - a bitmask
						length - the number of bits in bitmask
						locks - a mutes for each word of 8 bit in bitmask
						bit_position - the position in bitmask that shall be altered
						value - the binary value that shall be written to bit_position in
							bitmask		
*/
void write_to_bitmask(unsigned char *bitmask, int length, omp_lock_t *locks, int bit_position, int value) {

	# ifdef _OPENMP
		omp_set_lock(&locks[bit_position / 8]);
	# endif

	if (value) {

		bitmask[bit_position / 8] |= 1 << (bit_position % 8);

	}
	else {

		bitmask[bit_position / 8] &= ~(1 << (bit_position % 8));

	}

	# ifdef _OPENMP
		omp_unset_lock(&locks[bit_position / 8]);
	# endif

}
