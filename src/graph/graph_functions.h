void compactify_graph(int ***);
void resize_edge_list(int ***, int, int);

int * prefix_sum_on_deletion(int ***);

int get_path_in_directed_graph(int ***, int, int, int, int *);
void flip_directed_edge(int ***, int, int);

void clear_bitmask(unsigned char *, int);

void remove_isolated_vertices(int ***);
