#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>
#include "graph.h"
#include "graph_functions.h"
#include "graph_functions_parallel.h"
#include "../queue/queue.h"
#include "../vertex_list/vertex_list.h"

// Allows file global definition of work package size for each CPU.
#define WORK_PACKAGE_SIZE 64

extern int initial_edge_count;
extern int edge_enlargement;
extern int read_size;

extern int additional_graph_info;
extern int parameter;
extern int loops;
extern int start_parameter;
extern int edge_count;

/*
	compactify_graph
	Compactifies a graph.
	Params:		graph - a graph
*/
void compactify_graph(int ***graph) {

	int *prefix_sum = prefix_sum_on_deletion(graph);
	int *deleted = (int *)(malloc(get_graph_length(graph) * sizeof(int)));
	int *former_degree = (int *)(malloc(get_graph_length(graph) * sizeof(int)));
	int former_graph_length = get_graph_length(graph), new_graph_length = -1;
	int former_delta = -1;
	int delta = -1;
	int position = -1, list_position = -1, neighbor = -1;

	for (int v = 0; v < former_graph_length; v++) {

		deleted[v] = get_deleted_flag(graph, v);
		former_degree[v] = degree(graph, v);

	}

	for (int v = 0; v < former_graph_length; v++) {

		if (!deleted[v]) {

			position = prefix_sum[v] - 1;

			list_position = additional_graph_info;
			delta = former_degree[v];
			former_delta = delta;

			if (additional_graph_info + former_delta < initial_edge_count) delta = initial_edge_count - additional_graph_info;
			if (additional_graph_info + former_delta > graph[position][0][0]) {

				resize_edge_list(graph, position, delta);

			}

			// Add vertices to list.

			for (int j = additional_graph_info; j < graph[v][0][0]; j++) {

				graph[v][1][j] = -1;

				neighbor = graph[v][0][j];

				if (neighbor > -1 && !deleted[neighbor]) {

					graph[position][0][list_position] = prefix_sum[neighbor] - 1;
					list_position++;

				}

			}

			graph[position][0][0] = additional_graph_info + delta;
			graph[position][0][1] = 0;
			graph[position][0][2] = prefix_sum[former_graph_length - 1];
			graph[position][0][3] = graph[v][0][3];
			graph[position][0][4] = prefix_sum[former_graph_length - 1];

			// Fill with -1.

			if (additional_graph_info + former_delta < initial_edge_count) {

				for (int j = additional_graph_info + former_delta; j < initial_edge_count; j++) graph[position][0][j] = -1;

			}

			// Resize list.

			if (additional_graph_info + former_delta < graph[position][0][0]) {

				resize_edge_list(graph, position, delta);

			}

		}

	}

	graph[0][0][4] = prefix_sum[former_graph_length - 1];
	new_graph_length = prefix_sum[former_graph_length - 1];
	if (new_graph_length < 1) new_graph_length = 1;

	graph = (int ***)(realloc(graph, new_graph_length * sizeof(int**)));

	for (int v = 0; v < get_graph_length(graph); v++) {

		for (int j = additional_graph_info; j < graph[v][0][0]; j++) {

			neighbor = graph[v][0][j];

			if (neighbor > -1) {

				for (int k = additional_graph_info; k < graph[neighbor][0][0]; k++) {

					if (graph[neighbor][0][k] == v) {

						graph[neighbor][1][k] = j;
						graph[v][1][j] = k;
						break;

					}

				}

			}


		}

	}

	free(prefix_sum);
	free(deleted);
	free(former_degree);

}

/*
	resize_edge_list
	Resizes an edge list of a graph.
	Params:		graph - a graph
						position - the index of the vertex in graph who's edge list is to be
							resized
						delta - the amount of entries that will be added to the edge list of
							the vertex
	Throws:		an error with an explanatory message, if memory allocation fails
*/
void resize_edge_list(int ***graph, int position, int delta) {

	int *p = (int *)(realloc(graph[position][0], (additional_graph_info + delta) * sizeof(int)));
	if (!p) {
			printf("%i with size %i\n", position, (additional_graph_info + delta));
			exit(0);
	} else {
		graph[position][0] = p;
	}

	p = (int *)(realloc(graph[position][1], (additional_graph_info + delta) * sizeof(int)));
	if (!p) {
			printf("%i with size %i\n", position, (additional_graph_info + delta));
			exit(0);
	} else {
		graph[position][1] = p;
	}

}

/*
	prefix_sum_on_deletion
	Computes the prefix sum of the edge lists for each vertex in a graph. If a
		vertex is not deleted, it will be counted.
	Params:		graph - a graph
	Returns:	prefix_sum - an array of prefix sums, one prefix sum for each vertex
							in graph
*/
int * prefix_sum_on_deletion(int ***graph) {

	int *prefix_sum = (int *)(malloc(get_graph_length(graph) * sizeof(int)));

	prefix_sum[0] = 1 - get_deleted_flag(graph, 0);
	for (int i = 1; i < get_graph_length(graph); i++) {

		prefix_sum[i] = prefix_sum[i - 1] + (1 - get_deleted_flag(graph, i));

	}

	return prefix_sum;

}

/*
	get_path_in_directed_graph
	Determines a path in a directed graph.
	Params:		graph - a directed graph
						vertex_count - the total number of vertices in the graph
						s - the start vertex in the graph
						t - the target vertex in the graph
						path - an array in which the path will be inserted, the first element
							in the array holds info on the length of the path
	Returns:	1 if search was successfull, 0 otherwise
*/
int get_path_in_directed_graph(int ***graph, int vertex_count, int s, int t, int *path) {

	int *queue = create_queue(vertex_count);

	int *visited = (int *)(malloc(vertex_count * sizeof(int)));
	int *parent = (int *)(malloc(vertex_count * sizeof(int)));
	int current = -1;
	int other = -1;
	int *path_rev = (int *)(malloc((vertex_count + 1) * sizeof(int)));
	int cnt = 0;

	for (int i = 0; i < vertex_count; i++) {

		visited[i] = 0;
		parent[i] = -1;

	}

	// Using BFS for determination of path.

	push_queue(queue, s);

	while ((current = pop_queue(queue)) > -1) {

		visited[current] = 1;

		for (int i = additional_graph_info; i < graph[current][0][0]; i++) {

			other = graph[current][0][i];

			if(other > -1) {

				if (!get_deleted_flag(graph, other) && !visited[other]) {

					parent[other] = current;
					visited[other] = 1;
					push_queue(queue, other);


				}

			}

		}

	}

	current = t;
	cnt = 0;


	while (current != -1) {

		path_rev[cnt] = current;
		cnt++;

		current = parent[current];

	}

	path[0] = cnt;

	for (int i = 1; i <= cnt; i++) {

		path[i] = path_rev[cnt - i];

	}

	free(queue);
	free(visited);
	free(parent);
	free(path_rev);

	return (cnt > 1);

}

/*
	remove_isolated_vertices
	Removes any isolated vertices from a graph.
	Params:		graph - a graph
*/
void remove_isolated_vertices(int ***graph) {

	int *degrees = (int *)(malloc(get_graph_length(graph) * sizeof(int)));
  unsigned char *bitmask = (unsigned char *)(calloc((get_graph_length(graph) / 8 + 1),  sizeof(unsigned char)));
	# ifdef _OPENMP
		omp_lock_t *locks = (omp_lock_t *)(malloc((get_graph_length(graph) / 8 + 1) * sizeof(omp_lock_t)));
		for (int i = 0; i < (get_graph_length(graph) / 8 + 1); i++) {
				omp_init_lock(&locks[i]);
		}
	# endif

	#pragma omp parallel for schedule(static, WORK_PACKAGE_SIZE)
	for (int i = 0; i < get_graph_length(graph); i++) {

		degrees[i] = degree(graph, i);

	}

  #pragma omp parallel for schedule(static, WORK_PACKAGE_SIZE)
	for (int i = 0; i < get_graph_length(graph); i++) {


		if (degrees[i] == 0 && !get_deleted_flag(graph, i)) {

			# ifdef _OPENMP
				write_to_bitmask(bitmask, get_graph_length(graph), locks, i, 1);
			# else
				bitmask[i / 8] |= 1 << (i % 8);
			# endif

    }

	}

  remove_vertices_through_bitmask(graph, bitmask, get_graph_length(graph));

	free(degrees);
	free(bitmask);

	# ifdef _OPENMP
		free(locks);
	# endif

}

/*
	flip_directed_edge
	Flips a directed edge in a graph. If the edge actually is undirected,
		the graph remains unchanged.
	Params:		graph - a graph
						vertex0 - a first vertex
						vertex1 - a second vertex
*/
void flip_directed_edge(int ***graph, int vertex0, int vertex1) {

	if (is_directed_edge(graph, vertex0, vertex1) && !is_directed_edge(graph, vertex1, vertex0)) {

		remove_directed_edge(graph, vertex0, vertex1);
		add_directed_edge(graph, vertex1, vertex0);

	}
	else if (is_directed_edge(graph, vertex1, vertex0) && !is_directed_edge(graph, vertex0, vertex1)) {

		remove_directed_edge(graph, vertex1, vertex0);
		add_directed_edge(graph, vertex0, vertex1);

	}

}

/*
	clear_bitmask
	Sets every bit in a bitmask to zero.
	Params:		bitmask - a bitmask
						length - the number of bits in bitmask
*/
void clear_bitmask(unsigned char *bitmask, int length) {

	for (int i = 0; i < length / 8 + 1; i++) {

		bitmask[i] = 0;

	}

}
