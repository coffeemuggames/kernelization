#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <omp.h>
#include "graph.h"
#include "graph_builder.h"
#include "../queue/queue.h"
#include "../vertex_list/vertex_list.h"
#include "../utilities/time_utilities.h"


extern int initial_edge_count;
extern int edge_enlargement;
extern int read_size;

extern int additional_graph_info;
extern int parameter;
extern int loops;
extern int start_parameter;

int edge_count;

double computation_time, io_time, computation_cpu_time;
clock_t cpu_timer;

struct timespec computation_time_start, computation_time_end,
	io_time_start, io_time_end;

/*
	read_graph
	Reads a graph from a stream and saves it into an array.
	Params:		stream - a stream from which the graph will be read.
	Returns:	graph - a pointer to the graph.
*/
int *** read_graph(FILE *stream) {

	io_time = 0;
	computation_time = 0;
	computation_cpu_time = 0;
	start_timer(&io_time_start, &io_time_end);

	char *line = malloc(read_size * sizeof(char));
	char *token;
	char *delimiter = (char *)(malloc(3 * sizeof(char)));
	delimiter[0] = ' ';
	delimiter[1] = '\n';
	delimiter[2] = '\t';
	int start = -1, end = -1;
	float ct = 0;
	int ***graph;
	int *edge_list_index;
	int *vertex_degree;

	while (fgets(line, read_size, stream) != NULL) {

		token = strtok(line, delimiter);

		if (line[0] == 'p') {

			int vertex_cnt = 0;

			token = strtok(NULL, delimiter);
			token = strtok(NULL, delimiter);
			vertex_cnt = strtol(token, NULL, 10);
			token = strtok(NULL, delimiter);
			edge_count = strtol(token, NULL, 10);

			if (vertex_cnt == 0) vertex_cnt = 1;

			graph = create_graph(vertex_cnt);
			graph[0][0][2] = vertex_cnt;
			graph[0][0][3] = 0;
			graph[0][0][4] = vertex_cnt;

			edge_list_index = (int *)(calloc(get_graph_length(graph), sizeof(int)));
			vertex_degree = (int *)(calloc(get_graph_length(graph), sizeof(int)));

		}
		else if (line[0] == 'c') {

			token = strtok(NULL, delimiter);

			if (strcmp(token, "k") == 0) {

				token = strtok(NULL, delimiter);
				parameter = strtol(token, NULL, 10);
				start_parameter = parameter;

			}
			else if (strcmp(token, "l") == 0) {

				token = strtok(NULL, delimiter);
				loops = strtol(token, NULL, 10);

			}
			else if (strcmp(token, "time") == 0) {

				token = strtok(NULL, delimiter);

				if (strcmp(token, "computation") == 0) {

					token = strtok(NULL, delimiter);
					ct = strtod(token, NULL);
					computation_time += ct;

				}
				else if (strcmp(token, "cpu") == 0) {

					token = strtok(NULL, delimiter);
					ct = strtod(token, NULL);
					computation_cpu_time += ct;

				}
				else if (strcmp(token, "i/o") == 0) {

					token = strtok(NULL, delimiter);
					ct = strtod(token, NULL);
					io_time += ct;

				}

		}

		}
		else {

			start = strtol(token, NULL, 10);
			start--;
			token = strtok(NULL, delimiter);
			end = strtol(token, NULL, 10);
			end--;

			add_undirected_edge_fast(graph, vertex_degree, edge_list_index, start, end);


		}

	}

	io_time += stop_timer(&io_time_start, &io_time_end);

	start_timer(&computation_time_start, &computation_time_end);
	start_cpu_timer(cpu_timer);

	free(line);
	free(delimiter);
	free(vertex_degree);
	free(edge_list_index);

	return graph;

}

/*
	create_graph
	Creates a graph with a specified number of nodes.
	Params:		vertex_count - number of vertices in the graph
	Returns:	graph - a pointer to the newly created graph
*/
int *** create_graph(int vertex_count) {

	int ***graph = (int ***)(malloc(vertex_count * sizeof(int**)));

	for (int i = 0; i < vertex_count; i++) {

		graph[i] = (int **)(malloc(2 * sizeof(int*)));
		graph[i][0] = (int *)(malloc(initial_edge_count * sizeof(int)));
		graph[i][1] = (int *)(malloc(initial_edge_count * sizeof(int)));

		for (int j = 0; j < initial_edge_count; j++) {

			graph[i][0][j] = -1;
			graph[i][1][j] = -1;
			graph[i][0][3] = 0;

		}

		graph[i][0][0] = initial_edge_count;
		graph[i][0][1] = 0;
		graph[0][0][2] = vertex_count;
		graph[0][0][4] = vertex_count;

	}

	return graph;

}

/*
	free_graph
	Appropriately frees the memory a graph took up.
	Params:		graph - graph
*/
void free_graph(int ***graph) {

	int graph_length = get_graph_length(graph);

	for (int i = 0; i < graph_length; i++) {

		free(graph[i][0]);
		free(graph[i][1]);
		free(graph[i]);

	}

	free(graph);

}

/*
	output_graph
	Outputs an existing graph to standard output.
	Params:		graph - a graph
*/
void output_graph(int ***graph) {

	computation_cpu_time += stop_cpu_timer(cpu_timer);
	computation_time += stop_timer(&computation_time_start, &computation_time_end);
	start_timer(&io_time_start, &io_time_end);

	int this = -1, other = -1;
	int low_vertex = 1;
	int *names = (int *)(malloc(get_graph_length(graph) * sizeof(int)));

	edge_count = count_undirected_edges(graph);

	for (int i = 0; i < get_graph_length(graph); i++) {

		names[i] = -1;

	}

	printf("c k %i\n", parameter);
	printf("p tw %i %i\n", graph[0][0][2], edge_count);

	for (int i = 0; i < get_graph_length(graph); i++) {

		this = i;

		if (!get_deleted_flag(graph, this)) {

			for (int j = additional_graph_info; j < graph[this][0][0]; j++) {

				other = graph[this][0][j];

				if (other > this) {

					if (!get_deleted_flag(graph, other)) {

						if (names[this] == -1) {

							names[this] = low_vertex++;

						}

						if (names[other] == -1) {

							names[other] = low_vertex++;

						}

						printf("%i %i\n", names[this], names[other]);

					}

				}

			}

		}

	}


	io_time += stop_timer(&io_time_start, &io_time_end);
	printf("c time computation %f\n", computation_time);
	printf("c time cpu %f\n", computation_cpu_time);
	printf("c time i/o %f\n", io_time);

	free(names);

}

/*
	output_graph_raw
	Outputs an existing graph to standard output, without any metadata.
	Params:		graph - a graph
*/
void output_graph_raw(int ***graph) {

	int this = -1, other = -1;
	int low_vertex = 1;
	int *names = (int *)(malloc(get_graph_length(graph) * sizeof(int)));

	edge_count = count_undirected_edges(graph);

	for (int i = 0; i < get_graph_length(graph); i++) {

		names[i] = -1;

	}

	printf("p tw %i %i\n", graph[0][0][2], edge_count);

	for (int i = 0; i < get_graph_length(graph); i++) {

		this = i;

		if (!get_deleted_flag(graph, this)) {

			for (int j = additional_graph_info; j < graph[this][0][0]; j++) {

				other = graph[this][0][j];

				if (other > this) {

					if (!get_deleted_flag(graph, other)) {

						if (names[this] == -1) {

							names[this] = low_vertex++;

						}

						if (names[other] == -1) {

							names[other] = low_vertex++;

						}

						printf("%i %i\n", names[this], names[other]);

					}

				}

			}

		}

	}

	free(names);

}

/*
	output_graph_uncompactified
	Outputs an exisiting graph to standard output without
		renaming due to missing edges.
	Params:		graph - a graph
*/
void output_graph_uncompactified(int ***graph) {

	computation_cpu_time += stop_cpu_timer(cpu_timer);
	computation_time += stop_timer(&computation_time_start, &computation_time_end);
	start_timer(&io_time_start, &io_time_end);

	int other = -1;

	printf("c k %i\n", parameter);
	printf("p tw %i %i\n", graph[0][0][2], count_undirected_edges(graph));

	for (int this = 0; this < get_graph_length(graph); this++) {

		if (!get_deleted_flag(graph, this)) {

			for (int j = additional_graph_info; j < graph[this][0][0]; j++) {

				other = graph[this][0][j];

				if (other > this) {

					if (!get_deleted_flag(graph, other)) {

						printf("%i %i\n", this+1, other+1);

					}

				}

			}

		}

	}

	io_time += stop_timer(&io_time_start, &io_time_end);
	printf("c time computation %f\n", computation_time);
	printf("c time cpu %f\n", computation_cpu_time);
	printf("c time i/o %f\n", io_time);

}

/*
	output_directed_graph_uncompactified
	Outputs an exisiting directed graph to standard output without
		renaming due to missing edges. No output of vertex count
		or edge count.
	Params:		graph - a graph
*/
void output_directed_graph_uncompactified(int ***graph) {

	int other = -1;

	for (int this = 0; this < get_graph_length(graph); this++) {

		if (!get_deleted_flag(graph, this)) {

			for (int j = additional_graph_info; j < graph[this][0][0]; j++) {

				other = graph[this][0][j];

				if (other > -1) {

					if (!get_deleted_flag(graph, other)) {

						printf("%i %i\n", this+1, other+1);

					}

				}

			}

		}

	}

}

/*
	output_empty_graph
	Outputs an empty graph to standard output.
*/
void output_empty_graph() {

	computation_cpu_time += stop_cpu_timer(cpu_timer);
	computation_time += stop_timer(&computation_time_start, &computation_time_end);
	start_timer(&io_time_start, &io_time_end);

	printf("c k 1\n");
	printf("p tw 0 0\n");

	io_time += stop_timer(&io_time_start, &io_time_end);
	printf("c time computation %f\n", computation_time);
	printf("c time cpu %f\n", computation_cpu_time);
	printf("c time i/o %f\n", io_time);

}

/*
	output_yes_instance
	Outputs a trivial yes instance for vertex cover to standard output.
*/
void output_yes_instance() {

	computation_cpu_time += stop_cpu_timer(cpu_timer);
	computation_time += stop_timer(&computation_time_start, &computation_time_end);
	start_timer(&io_time_start, &io_time_end);

	printf("c k 1\n");
	printf("p tw 2 1\n");
	printf("1 2\n");

	io_time += stop_timer(&io_time_start, &io_time_end);
	printf("c time computation %f\n", computation_time);
	printf("c time cpu %f\n", computation_cpu_time);
	printf("c time i/o %f\n", io_time);

}

/*
	output_no_instance
	Outputs a trivial no instance for vertex cover to standard output.
*/
void output_no_instance() {

	computation_cpu_time += stop_cpu_timer(cpu_timer);
	computation_time += stop_timer(&computation_time_start, &computation_time_end);
	start_timer(&io_time_start, &io_time_end);

	printf("c k 0\n");
	printf("p tw 2 1\n");
	printf("1 2\n");

	io_time += stop_timer(&io_time_start, &io_time_end);
	printf("c time computation %f\n", computation_time);
	printf("c time cpu %f\n", computation_cpu_time);
	printf("c time i/o %f\n", io_time);

}

/*
	output_vc
	Outputs a vertex cover of a graph.
	Params:		graph - a graph
						deleted_vertices - a list indicating, which vertices belong to the
							vertex cover
						vc_size - the size of the vertex cover induced by deleted_vertices
*/
void output_vc(int ***graph, int *deleted_vertices, int vc_size) {

	computation_cpu_time += stop_cpu_timer(cpu_timer);
	computation_time += stop_timer(&computation_time_start, &computation_time_end);
	start_timer(&io_time_start, &io_time_end);

  printf("p tw %i 0\n", vc_size);

  for (int i = 0; i < get_graph_length(graph); i++) {

    if (deleted_vertices[i]) {

      printf("%i\n", i + 1);

    }

  }

	io_time += stop_timer(&io_time_start, &io_time_end);
	printf("c time computation %f\n", computation_time);
	printf("c time cpu %f\n", computation_cpu_time);
	printf("c time i/o %f\n", io_time);

}

/*
	output_no_vc
	Outputs a message suitable for usage in a pipeline with kernelization
		and the get_stats statistics generation script, that indicates, that
		no vertex cover could be found.
	Params:		graph - a graph
*/
void output_no_vc(int ***graph) {

	computation_cpu_time += stop_cpu_timer(cpu_timer);
	computation_time += stop_timer(&computation_time_start, &computation_time_end);
	start_timer(&io_time_start, &io_time_end);

  printf("p tw -1 0\n");

	io_time += stop_timer(&io_time_start, &io_time_end);
	printf("c time computation %f\n", computation_time);
	printf("c time cpu %f\n", computation_cpu_time);
	printf("c time i/o %f\n", io_time);

}

/*
	output_full_graph_vc
	Outputs a message suitable for usage in a pipeline with kernelization
		and the get_stats statistics generation script, that indicates, that
		the graph as a whole is a valid vertex cover.
	Params:		graph - a graph
*/
void output_full_graph_vc(int ***graph) {

	computation_cpu_time += stop_cpu_timer(cpu_timer);
	computation_time += stop_timer(&computation_time_start, &computation_time_end);
	start_timer(&io_time_start, &io_time_end);

  printf("p tw %i 0\n", get_graph_length(graph));

  for (int i = 0; i < get_graph_length(graph); i++) {

    printf("%i\n", i + 1);

  }

	io_time += stop_timer(&io_time_start, &io_time_end);
	printf("c time computation %f\n", computation_time);
	printf("c time cpu %f\n", computation_cpu_time);
	printf("c time i/o %f\n", io_time);

}
