int get_graph_length(int ***);
int get_next_neighbor(int ***, int, int *);

int is_undirected_edge(int ***, int, int);
int is_directed_edge(int ***, int, int);
int has_edge(int ***, int);
int degree(int ***, int);

void remove_vertex(int ***, int);
void remove_vertices_through_bitmask(int ***, unsigned char *, int);
void remove_all_vertices(int ***);

void add_undirected_edge(int ***, int, int);
void add_undirected_edge_fast(int ***, int *, int *, int, int);
int add_directed_edge(int ***, int, int);
int add_directed_edge_fast(int ***, int *, int *, int, int);
void remove_undirected_edge(int ***, int, int);
void remove_directed_edge(int ***, int, int);
void adjust_adjacency_list(int ***, int);
void adjust_adjacency_list_forced(int ***, int);

int count_vertices(int ***);
int count_undirected_edges(int ***);

void set_position_flag(int ***, int, int);
int get_position_flag(int ***, int);
void set_deleted_flag(int ***, int, int);
int get_deleted_flag(int ***, int);
