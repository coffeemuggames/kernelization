#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "graph_append.h"

static int read_size = 1024;

int main(int argc, char **argv) {

	char *line = malloc(read_size * sizeof(char));
	char *l = malloc(read_size * sizeof(char));
	char *token;
	char *delimiter = (char *)(malloc(3 * sizeof(char)));
	delimiter[0] = ' ';
	delimiter[1] = '\n';
	delimiter[2] = '\t';

	if (argc > 1) {

		printf("c k %s\n", argv[1]);

	}

	if (argc > 2) {

		printf("c l %s\n", argv[2]);

	}

	while (fgets(line, read_size, stdin) != NULL) {

		strcpy(l, line);
		token = strtok(l, delimiter);		

		if (!strcmp(token, "c")) {

			token = strtok(NULL, delimiter);

			if (strcmp(token, "k")) {

				printf("%s", line);

			}

		}
		else {

			printf("%s", line);

		}

	}

	return 0;

}
