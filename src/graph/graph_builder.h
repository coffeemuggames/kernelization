int *** read_graph(FILE *);
int *** create_graph(int);
void free_graph(int ***);

void output_graph(int ***);
void output_graph_raw(int ***);
void output_graph_uncompactified(int ***);
void output_directed_graph_uncompactified(int ***);
void output_empty_graph();
void output_yes_instance();
void output_no_instance();

void output_vc(int ***graph, int *, int);
void output_full_graph_vc(int ***);
void output_no_vc(int ***);
