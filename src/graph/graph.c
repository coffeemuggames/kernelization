#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "graph.h"
#include "graph_functions.h"
#include "../queue/queue.h"
#include "../vertex_list/vertex_list.h"

const int initial_edge_count = 8;
int edge_enlargement = 32;
const int read_size = 1024;

/*
	Note that in memory some additional meta-data of the graph is
		stored. If you are iterating over the graph, you should start
		at position additional_graph_info.
*/
int additional_graph_info = 5; // additional info consists of arraylength, delete_flag, vertex_count, position flag, graph_length
int parameter = 10; // standard value is 10
int loops = 1; // standard value is 1
int start_parameter = 10;
int edge_count = -1;

/*
	get_graph_length
	Gets the number of vertices stored in a graph (regardless of deleted
		or not).
	Params:		graph - graph
	Returns: 	graph_length - the number of vertices stored in graph
*/
int get_graph_length(int ***graph) {

	return graph[0][0][4];

}

/*
	get_next_neighbor
	Iterates over the neighbors of a vertex.
	Params:		graph - a graph
						vertex - a vertex in the graph
						iterator - a pointer to an int, initialized with 0
	Returns:	neighbor - another neighbor of vertex, -1 if no more
							neighbors exist
*/
int get_next_neighbor(int ***graph, int vertex, int *iterator) {

	while (additional_graph_info + *iterator < graph[vertex][0][0]
		&& (graph[vertex][0][additional_graph_info + *iterator] == -1
		|| get_deleted_flag(graph, graph[vertex][0][additional_graph_info + *iterator]))) {

		*iterator = *iterator + 1;

	}

	if (additional_graph_info + *iterator <	graph[vertex][0][0]) {

		*iterator = *iterator + 1;
		return graph[vertex][0][additional_graph_info + *iterator - 1];

	}
	else {

		return -1;

	}

}

/*
	add_undirected_edge
	Adds an undirected edge to an existing graph.
	Params:		graph - a graph
						vertex0 - the first vertex
						vertex1 - the second vertex
*/
void add_undirected_edge(int ***graph, int vertex0, int vertex1) {

	int index0 = -1, index1 = -1;

	index0 = add_directed_edge(graph, vertex0, vertex1);
	index1 = add_directed_edge(graph, vertex1, vertex0);

	graph[vertex0][1][index0] = index1;
	graph[vertex1][1][index1] = index0;

}

void add_undirected_edge_fast(int ***graph, int *vertex_degree, int *edge_list_index, int start, int end) {

	if (vertex_degree[start] + additional_graph_info >= graph[start][0][0]) {
		adjust_adjacency_list_forced(graph, start);
	}
	if (vertex_degree[end] + additional_graph_info >= graph[end][0][0]) {
		adjust_adjacency_list_forced(graph, end);
	}

	graph[start][0][additional_graph_info + edge_list_index[start]] = end;
	graph[end][0][additional_graph_info + edge_list_index[end]] = start;
	graph[start][1][additional_graph_info + edge_list_index[start]] = additional_graph_info + edge_list_index[end];
	graph[end][1][additional_graph_info + edge_list_index[end]] = additional_graph_info + edge_list_index[start];

	edge_list_index[start]++;
	edge_list_index[end]++;
	vertex_degree[start]++;
	vertex_degree[end]++;

}

/*
	adjust_adjacency_list
	Updated the length of the adjacency list of a specified vertex under the
		assumption that another incident edge will be added soon. This function
		will check if it useful to enlarge the edge list.
	Params:		graph - a graph
						vertex - the vertex
*/
void adjust_adjacency_list(int ***graph, int vertex) {

	if (degree(graph, vertex) + additional_graph_info >= graph[vertex][0][0]) {

		// Enlarge the edge list for this vertex.

		adjust_adjacency_list_forced(graph, vertex);

	}

}

/*
	adjust_adjacency_list
	Updated the length of the adjacency list of a specified vertex under the
		assumption that another incident edge will be added soon. This functions
		will instantly enlarge the edge list without any further investigation.
	Params:		graph - a graph
						vertex - the vertex
*/
void adjust_adjacency_list_forced(int ***graph, int vertex) {

	graph[vertex][1] = (int *)realloc(graph[vertex][1], (graph[vertex][0][0] + edge_enlargement) * sizeof(int));
	graph[vertex][0] = (int *)realloc(graph[vertex][0], (graph[vertex][0][0] + edge_enlargement) * sizeof(int));

	for (int i = graph[vertex][0][0]; i < graph[vertex][0][0] + edge_enlargement; i++) {

		graph[vertex][0][i] = -1;
		graph[vertex][1][i] = -1;

	}

	graph[vertex][0][0] = graph[vertex][0][0] + edge_enlargement;

}

/*
	add_directed_edge
	Adds a directed edge to an existing graph.
	Params:		graph - a graph
						start - the start vertex
						end - the end vertex
	Returns:	index - the index of the edge list of the start vertex,
							where the edge entry was made
*/
int add_directed_edge(int ***graph, int start, int end) {

	int index = -1;

	// Update adjacency lists.
	adjust_adjacency_list(graph, start);

	//Add adjacency and increase edge count for second vertex.

	for (int i = additional_graph_info; i < graph[start][0][0]; i++) {

		if (graph[start][0][i] == -1) {

			graph[start][0][i] = end;
			graph[start][1][i] = -1;
			index = i;
			break;

		}

	}

	return index;

}

/*
	add_directed_edge
	Adds a directed edge to an existing graph, but faster.
	Params:		graph - a graph
						vertex_degree - a list holding vertex degree information
							for every vertex
						edge_list_index - a list holding information on the current index in
							the edge list for every vertex
						start - the start vertex
						end - the end vertex
	Returns:	index - the index of the edge list of the start vertex,
							where the edge entry was made
*/
int add_directed_edge_fast(int ***graph, int *vertex_degree, int *edge_list_index, int start, int end) {

	int index = -1;

	// Update adjacency lists.
	if (vertex_degree[start] + additional_graph_info >= graph[start][0][0]) {
		adjust_adjacency_list_forced(graph, start);
	}

	//Add adjacency and increase edge count for second vertex.

	graph[start][0][additional_graph_info + edge_list_index[start]] = end;
	graph[start][1][ additional_graph_info + edge_list_index[start]] = -1;
	index = additional_graph_info + edge_list_index[start];

	edge_list_index[start]++;
	vertex_degree[start]++;

	return index;

}

/*
	remove_undirected_edge
	Removes an undirected edge from an existing graph.
	Params:		graph - a graph
						start - the start vertex
						end - the end vertex
*/
void remove_undirected_edge(int ***graph, int start, int end) {

	int host = start, partner = end;

	if (start != -1 && end != -1)  {

		if (graph[start][0][0] > graph[end][0][0]) {

			host = end;
			partner = start;

		}

		for (int i = additional_graph_info; i < graph[host][0][0]; i++) {

			if (graph[host][0][i] == partner) {

				// Use cached index of entry in other vertex for quick removal.

				graph[partner][0][graph[host][1][i]] = -1;
				graph[partner][1][graph[host][1][i]] = -1;
				graph[host][0][i] = -1;
				graph[host][1][i] = -1;
				break;

			}

		}

	}

}

/*
	remove_directed_edge
	Removes a directed edge from an existing graph.
	Params:		graph - a graph
						start - the start vertex
						end - the end vertex
*/
void remove_directed_edge(int ***graph, int start, int end) {

	for (int i = additional_graph_info; i < graph[start][0][0]; i++) {

		if (graph[start][0][i] == end) {

			graph[start][0][i] = -1;
			graph[start][1][i] = -1;
			break;

		}

	}

}

/*
	remove_vertex
	Removes a vertex from an existing graph.
	Params:		graph - a graph
						vertex - the vertex to be removed
*/
void remove_vertex(int ***graph, int vertex) {

	int other = -1, index = -1;

	if (!get_deleted_flag(graph, vertex)) {

		set_deleted_flag(graph, vertex, 1);

		#pragma omp atomic
		graph[0][0][2]--;

		for (int i = additional_graph_info; i < graph[vertex][0][0]; i++) {

			other = graph[vertex][0][i];

			if (other > -1) {

				index = graph[vertex][1][i];

				if (index > -1) {

					graph[other][0][index] = -1;
					graph[other][1][index] = -1;
					graph[vertex][0][i] = -1;
					graph[vertex][1][i] = -1;

				}

			}

		}

	}

}

/*
	remove_vertices_through_bitmask
	Removes vertices from a graph using a bitmask.
	Params:		graph - a graph
						bitmask - a bitmask array (1 = delete, 0 = do not delete)
						bitmask_length - number of bits to read from bitmask
*/
void remove_vertices_through_bitmask(int ***graph, unsigned char *bitmask, int bitmask_length) {

	unsigned int mask = 1;
	int count = 0;

	for (int i = 0; i < bitmask_length; i++) {

		if (i % 8 == 0) {

			mask = 1;

		}
		else {

			mask = mask << 1;

		}

		if (bitmask[i / 8] & mask) {

			remove_vertex(graph, i);
			count++;

		}

	}

}

/*
	remove_all_vertices
	Removes all vertices from a graph.
	Params:		graph - a graph
*/
void remove_all_vertices(int ***graph) {

	for (int i = 0; i < get_graph_length(graph); i++) {

		set_deleted_flag(graph, i, 1);

	}

	graph[0][0][2] = 0;

}

/*
	count_vertices
	Counts the number of vertices that are still left.
	Params:		graph - a graph
	Returns:	number - the number of vertices
*/
int count_vertices(int ***graph) {

	return graph[0][0][2];

}

/*
	count_undirected_edges
	Counts the number of undirected edges that are still left. Only functions
		correctly when number of undirected edges are looked up.
	Params:		graph - a graph
	Returns:	number - the number of edges
*/
int count_undirected_edges(int ***graph) {

	int count = 0;
	int other = -1;

	for (int i = 0; i < get_graph_length(graph); i++) {

		if (!get_deleted_flag(graph, i)) {

			for (int j = additional_graph_info; j < graph[i][0][0]; j++) {

				other = graph[i][0][j];

				if (other > -1 && !get_deleted_flag(graph, other)) {

					#pragma omp atomic
					count++;

				}

			}

		}

	}

	return count / 2;

}

/*
	is_directed_edge
	Checks whether a specified directed edge does exist.
	Params:		graph - a graph
						vertex0 - the start vertex
						vertex1 - the end vertex
	Returns:	1 if the edge exists, 0 otherwise
*/
int is_directed_edge(int ***graph, int vertex0, int vertex1) {

	// Check if either vertex is already deleted.
	if (get_deleted_flag(graph, vertex0) || get_deleted_flag(graph, vertex1)) {

		return 0;

	}
	else {

		for (int i = additional_graph_info; i < graph[vertex0][0][0]; i++) {

			if (graph[vertex0][0][i] == vertex1) {

				return 1;

			}

		}

		return 0;

	}

}

/*
	is_undirected_edge
	Checks whether a specified undirected edge does exist.
	Params:		graph - a graph
						vertex0 - the start vertex
						vertex1 - the end vertex
	Returns:	1 if the edge exists, 0 otherwise
*/
int is_undirected_edge(int ***graph, int vertex0, int vertex1) {

	int host = vertex0;
	int partner = vertex1;

	// Check if either vertex is already deleted.
	if (get_deleted_flag(graph, vertex0) || get_deleted_flag(graph, vertex1)) {

		return 0;

	}
	else {

		if (graph[vertex1][0][0] < graph[vertex0][0][0]) {

			host = vertex1;
			partner = vertex0;

		}

		for (int i = additional_graph_info; i < graph[host][0][0]; i++) {

			if (graph[host][0][i] == partner) {

				return 1;

			}

		}

		return 0;

	}

}

/*
	degree
	Params:		graph - a graph
						vertex - a vertex
	Returns: the degree of the specified vertex
*/
int degree(int ***graph, int vertex) {

	int degree = 0;

	for (int i = additional_graph_info; i < graph[vertex][0][0]; i++) {

		if (graph[vertex][0][i] > -1 && !get_deleted_flag(graph, graph[vertex][0][i])) {

			#pragma omp atomic
			degree++;

		}

	}

	return degree;

}

/*
	set_position_flag
	Sets the position flag on a vertex.
	Params:		graph - a graph
						vertex - a vertex in the graph
						flag - the flag that shall be used
*/
void set_position_flag(int ***graph, int vertex, int flag) {

	graph[vertex][0][3] = flag;

}

/*
	get_position_flag
	Gets the position flag on a vertex.
	Params:		graph - a graph
						vertex - a vertex in the graph
	Returns:	flag - the position flag of the vertex
*/
int get_position_flag(int ***graph, int vertex) {

	return graph[vertex][0][3];

}

/*
	set_deleted_flag
	Sets the deleted flag of a vertex.
	Params:		graph - graph
						vertex - a vertex in graph
						flag - the value for the deleted flag
*/
void set_deleted_flag(int ***graph, int vertex, int flag) {

	graph[vertex][0][1] = flag;

}

/*
	get_deleted_flag
	Gets the deleted flag of a vertex.
	Params:		graph - a graph
						vertex - a vertex in graph
	Returns:	deleted - 1 if vertex is deleted, 0 otherwise
*/
int get_deleted_flag(int ***graph, int vertex) {

	return graph[vertex][0][1];

}

/*
	has_edge
	Checks if a vertex has at least one incident edge.
	Params:		graph - a graph
						vertex - a vertex in the graph
	Returns:	Indication on the question.
*/
int has_edge(int ***graph, int vertex) {

	int cnt = 0;

	for (int i = additional_graph_info; i < graph[vertex][0][0]; i++) {

		if (graph[vertex][0][i] > -1) {

			cnt = 1;
			break;

		}

	}

	return cnt;

}
