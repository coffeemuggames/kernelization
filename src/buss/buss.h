#define SHOW_GRAPH 0
#define DELETE_VERTICES 1
#define DEBUG -1

void delete_vertices(int ***);
int * get_sorted_vertices(int *, int *, int);
void show_graph(int *, int *);
void decide_result(int ***);
