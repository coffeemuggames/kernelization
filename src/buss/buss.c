#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <omp.h>
#include "buss.h"
#include "../graph/graph.h"
#include "../graph/graph_builder.h"
#include "../graph/graph_functions.h"
#include "../graph/graph_functions_parallel.h"
#include "../utilities/time_utilities.h"

// Allows for usage of only a percentage of the cpus (use multiples of 2).
#define CPU_USAGE_FACTOR 1

// Allows file global definition of work package size for each CPU.
#define WORK_PACKAGE_SIZE 32

extern int parameter;
extern int start_parameter;
extern int loops;
int ***graph;

int main(int argc, char **argv) {

	# ifdef _OPENMP
		omp_set_num_threads(omp_get_num_procs() / CPU_USAGE_FACTOR);
	# endif

	graph = read_graph(stdin);

	if (parameter < loops) {

		loops = parameter;

	}

	if (loops <= 0) {

		loops = 1;

	}

	if (start_parameter >=  get_graph_length(graph)) {

		output_yes_instance();

	}
	else {

		delete_vertices(graph);
		decide_result(graph);

	}

	free_graph(graph);

	return 0;

}

/*
	delete_vertices
	Deletes any unwanted vertices in respect to Buss' kernelization.
	Params: 	graph - a graph
*/
void delete_vertices(int ***graph) {

	int vertex_diff = get_graph_length(graph);
	int old_parameter = -1;
	unsigned char *bitmask = (unsigned char *)(malloc((get_graph_length(graph) / 8 + 1) * sizeof(unsigned char)));
	# ifdef _OPENMP
		omp_lock_t *locks = (omp_lock_t *)(malloc((get_graph_length(graph) / 8 + 1) * sizeof(omp_lock_t)));
	# endif

	/* Getting vertex degrees before hand reduces side effects
		but lowers the efficiency.
	*/
	int *degrees = (int *)(malloc(get_graph_length(graph) * sizeof(int)));

	# ifdef _OPENMP
		for (int i = 0; i < (get_graph_length(graph) / 8 + 1); i++) {
				omp_init_lock(&locks[i]);
		}
	# endif

	for (int cnt = 0; cnt < loops; cnt++) {

		// Check if any reduction is still being made.

		if (old_parameter == parameter || parameter < 0) {

			break;

		}
		else {

			old_parameter = parameter;


		}

		clear_bitmask(bitmask, get_graph_length(graph));

		#pragma omp parallel for schedule(static, WORK_PACKAGE_SIZE)
		for (int i = 0; i < get_graph_length(graph); i++) {

			degrees[i] = degree(graph, i);

		}

		#pragma omp parallel for schedule(static, WORK_PACKAGE_SIZE)
		for (int i = 0; i < get_graph_length(graph); i++) {

			if (degrees[i] > parameter) {

				# ifdef _OPENMP
					write_to_bitmask(bitmask, get_graph_length(graph), locks, i, 1);
				# else
					bitmask[i / 8] |= 1 << (i % 8);
				# endif

			}

		}

		remove_vertices_through_bitmask(graph, bitmask, get_graph_length(graph));

		// Reduce parameter size due to selected vertices for vertex-cover.

		vertex_diff = vertex_diff - count_vertices(graph);
		parameter = parameter - vertex_diff;
		vertex_diff = count_vertices(graph);

	}

	remove_isolated_vertices(graph);

	# ifdef _OPENMP
		free(locks);
	# endif

	free(bitmask);
	free(degrees);

}

/*
	decide_result
	Decides whether the actual graph or an empty graph
	is put out as the result.
	Params:		graph - a graph
*/
void decide_result(int ***graph) {

	long long param_squared = (long long)parameter * (long long)parameter + (long long)parameter;

	if (parameter < 0 || count_vertices(graph) > param_squared) {

		output_no_instance();

	}
	else {

		output_graph(graph);

	}

}
