#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>
#include "vertex_list.h"

int additional_vertex_list_info = 1; // length of list is stored

/*

	create_vertex_list
	Creates a list of vertices of a specified length.
	Params:		length - the max length
	Returns:	vertex_list - the list

*/
int * create_vertex_list(int length) {

	int *vertex_list = (int *)(malloc((length + 1) * sizeof(int)));

	for (int i = 0; i < length + additional_vertex_list_info; i++) {

		vertex_list[i] = -1;

	}

	vertex_list[0] = length;

	return vertex_list;

}

/*
	add_vertex
	Adds a vertex to a vertex list without creating duplicates.
	Params:		vertex_list - a list of vertices
						vertex - a vertex
*/
void add_vertex_to_list(int *vertex_list, int vertex) {

	for (int i = additional_vertex_list_info; i < additional_vertex_list_info + vertex_list[0]; i++) {

		if (vertex_list[i] == vertex) {

			return;

		}

	}

	for (int i = additional_vertex_list_info; i < additional_vertex_list_info + vertex_list[0]; i++) {

		#pragma omp critical
		{

			if (vertex_list[i] == -1) {

				vertex_list[i] = vertex;

			}

		}

		if (vertex_list[i] == vertex) {

			break;

		}

	}

}

/*
	get_vertex_from_list_by_index
	Gets a vertex from a vertex list specified by index.
	Params:		vertex_list - a vertex list
						index - an index in the vertex_list
	Returns:	vertex - the desired vertex
*/
int get_vertex_from_list_by_index(int *vertex_list, int index) {

	return vertex_list[additional_vertex_list_info + index];

}

/*
	remove_vertex_from_list
	Removes a vertex from a vertex list.
	Params:		vertex_list - a vertex list
						vertex - a vertex
*/
void remove_vertex_from_list(int *vertex_list, int vertex) {

	for (int i = additional_vertex_list_info; i < additional_vertex_list_info + vertex_list[0]; i++) {

		if (vertex_list[i] == vertex) {

			vertex_list[i] = -1;

		}

	}

}

/*
	is_in_list
	Checks if a vertex is in a vertex list.
	Params:		vertex_list - a list of vertices
						vertex - a vertex
	Returns:	zero, if the vertex was not found, unequal zero otherwise
*/

int is_in_list(int *vertex_list, int vertex) {

	for (int i = additional_vertex_list_info; i < additional_vertex_list_info + vertex_list[0]; i++) {

		if (vertex_list[i] == vertex) {

			return 1;

		}

	}

	return 0;

}

/*
	elements_in_list
	Computes the size of a vertex list.
	Params:		vertex_list - a vertex list
	Returns:	size - the size of vertex_list
*/
int elements_in_list(int *vertex_list) {

	int cnt = 0;

	for (int i = additional_vertex_list_info; i < additional_vertex_list_info + vertex_list[0]; i++) {

		if (vertex_list[i] != -1) {

			cnt++;

		}

	}

	return cnt;

}

/*
		length_of_list
		Returns the number of elements a list can possibly hold.
		Params:		vertex_list - a vertex list
		Returns:	length - the length of the list for vertices
*/
int length_of_list(int *vertex_list) {

	return vertex_list[0];

}

/*
	clear
	Deletes all vertices from a vertex list.
	Params:		vertex_list - a vertex_list
*/
void clear_list(int *vertex_list) {

	for (int i = additional_vertex_list_info; i < additional_vertex_list_info + vertex_list[0]; i++) {

		vertex_list[i] = -1;

	}

}

/*
  print_list
  Outputs a list to stdout.
  Params: 	list - a list
          	len - the length of list
*/
void print_list(int *list, int len) {

  printf("   ");
  for (int i = 0; i < len; i++) {

    printf("%i ", list[i]);

  }
  printf("\n");

}
