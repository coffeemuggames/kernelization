int * create_vertex_list(int);
void add_vertex_to_list(int *, int);
int get_vertex_from_list_by_index(int *, int);
void remove_vertex_from_list(int *, int);
int is_in_list(int *, int);
int elements_in_list(int *);
int length_of_list(int *);
void clear_list(int *);
void print_list(int *, int);
