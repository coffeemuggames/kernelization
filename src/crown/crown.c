#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <omp.h>

#include "../graph/graph.h"
#include "../graph/graph_builder.h"
#include "../graph/graph_functions.h"
#include "../matching/matching.h"
#include "../matching/matching_functions.h"
#include "../matching/matching_functions_parallel.h"
#include "../vertex_list/vertex_list.h"
#include "../utilities/time_utilities.h"
#include "crown.h"
#include "crown_functions.h"

// Allows for usage of only a percentage of the cpus (use multiples of 2).
#define CPU_USAGE_FACTOR 1

extern int parameter;
extern int additional_graph_info;
int ***graph;
int ***matching0;
int ***matching1;
int ***bipartition;
int *matched;

int main() {

  // Initialize randomness.
  time_t t;
  time(&t);
  srand((unsigned) time(&t));

  # ifdef _OPENMP
    omp_set_num_threads(omp_get_num_procs() / CPU_USAGE_FACTOR);
  # endif

  int graph_trivial = 0;

  graph = read_graph(stdin);

  remove_isolated_vertices(graph);

  if (count_vertices(graph) <= parameter) {

    graph_trivial = 1;
    output_yes_instance();

  }

	while (count_vertices(graph) > 3 * parameter) {

		initialize_graphs();

		compute_maximal_matching(graph, matching0);

    fprintf(stderr, "computed maximal matching\n");

		if (count_undirected_edges(matching0) > parameter) {

      fprintf(stderr, "matching > parameter\n");

      graph_trivial = 1;
	    output_no_instance();
      finish_graphs();
      break;

		}
		else {

      fprintf(stderr, "computing crown\n");

		  prepare_crown_decomposition(graph, bipartition, matching0, matching1);

		  if (decide_crown_decomposition()) {

		      output_no_instance();
          graph_trivial = 1;
          finish_graphs();
          break;

	    }

		}

    if (parameter == 0 && count_undirected_edges(graph) > 0) {

      output_no_instance();
      graph_trivial = 1;

    }
    else if (parameter >= count_undirected_edges(graph)) {

      output_yes_instance();
      graph_trivial = 1;

    }

    finish_graphs();

	}

  if (!graph_trivial) {

    output_graph(graph);

  }

  free_graph(graph);

	return 0;

}

/*
	initialize_graph
	Initializes all the graphs that are used through crown decompositions.
*/
void initialize_graphs() {

	// Graph for maximal matching.

	matching0 = create_graph(get_graph_length(graph));

	// Graphs for maximum size matching.

	matching1 = create_graph(get_graph_length(graph));
	bipartition = create_graph(get_graph_length(graph));

}

/*
  finish_graphs
  Finishes up the graphs that have been initialized earlier on.
*/
void finish_graphs() {

  free_graph(matching0);
  free_graph(matching1);
  free_graph(bipartition);

}

/*
	decide_crown_decomposition
	Decides the actual crown decomposition problem based on
		graph operations made earlier on.
  Returns: 1 if instance was solved, 0 otherwise.
*/
int decide_crown_decomposition() {

	if (count_undirected_edges(matching1) > parameter) {

    return 1;

	}
	else {

    int reduction = 0;

    # ifdef _OPENMP
		  reduction = invoke_crown_removal_parallel(graph, bipartition, matching1);
    # else
      reduction = invoke_crown_removal(graph, bipartition, matching1);
    # endif

    parameter = parameter - reduction;
    remove_isolated_vertices(graph);

    return 0;

	}

}
