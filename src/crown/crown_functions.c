#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <omp.h>

#include "../graph/graph.h"
#include "../graph/graph_builder.h"
#include "../graph/graph_functions.h"
#include "../graph/graph_functions_parallel.h"
#include "../matching/matching.h"
#include "../matching/matching_functions.h"
#include "../matching/matching_functions_parallel.h"
#include "../vertex_list/vertex_list.h"
#include "../utilities/time_utilities.h"
#include "crown_functions.h"

// Allows for usage of only a percentage of the cpus (use multiples of 2).
#define CPU_USAGE_FACTOR 1

// Allows file global definition of work package size for each CPU.
#define WORK_PACKAGE_SIZE 512

extern int additional_graph_info;
extern int additional_queue_info;

/*
	prepare_crown_decomposition
	Computes matchings etc. for the eventual crown decomposition.
*/
void prepare_crown_decomposition(int ***graph, int ***bipartition, int ***matching0, int ***matching1) {

	# ifdef _OPENMP
		obtain_bipartition_from_matching(graph, matching0, bipartition);
	# else
		obtain_bipartition_from_matching(graph, matching0, bipartition);
	# endif

  remove_isolated_vertices(bipartition);

  # ifdef _OPENMP
    push_relabel_parallel_preinit(bipartition, matching1);
  # else
    push_relabel_nonparallel(bipartition, matching1);
  # endif

}

/*
	invoke_crown_removal
	Determines a crown in a graph and removes it.
  Params: 	graph - a graph
          	bipartition - a bipartition
          	matching1 - a maximum matching in the bipartition
  Returns:  crown_value - the number of vertices by which the parameter should
              be reduced
*/
int invoke_crown_removal(int ***graph, int ***bipartition, int ***matching1) {

	int *vertex_cover = compute_min_vc_from_bipartition_and_matching(bipartition, matching1);

	int current = -1;
  int elements_in_head = 0;

  unsigned char *bitmask = (unsigned char *)(calloc((get_graph_length(graph) / 8 + 1), sizeof(unsigned char)));

	for (int i = additional_queue_info; i < get_graph_length(graph); i++) {

		current = vertex_cover[i];

		if (current > -1 && !get_deleted_flag(graph, current) && get_position_flag(bipartition, current) == 2) {

				bitmask[current / 8] |= 1 << (current % 8);
				elements_in_head++;

		}

	}

  remove_vertices_through_bitmask(graph, bitmask, get_graph_length(graph));

  free(bitmask);
  free(vertex_cover);

	return elements_in_head;

}

/*
	invoke_crown_removal_parallel
	Determines a crown in a graph and removes it in parallel.
	Params: 	graph - a graph
        		bipartition - a bipartition
        		matching1 - a maximum matching in the bipartition
  Returns:  crown_value - the number of vertices by which the parameter should
              be reduced
*/
int invoke_crown_removal_parallel(int ***graph, int ***bipartition, int ***matching1) {

	int *vertex_cover = compute_min_vc_from_bipartition_and_matching(bipartition, matching1);

	int current = -1;
  int elements_in_head = 0;
	int *elements_in_head_thread = NULL;

  unsigned char *bitmask = (unsigned char *)(calloc((get_graph_length(graph) / 8 + 1), sizeof(unsigned char)));

	# ifdef _OPENMP
		omp_lock_t *locks = (omp_lock_t *)(malloc((get_graph_length(graph) / 8 + 1) * sizeof(omp_lock_t)));
		# pragma omp parallel for
		for (int i = 0; i < (get_graph_length(graph) / 8 + 1); i++) {
				omp_init_lock(&locks[i]);
		}
		elements_in_head_thread = (int *)(calloc(omp_get_num_procs(), sizeof(int)));
	# endif

	# pragma omp for private(current) schedule(static, WORK_PACKAGE_SIZE)
	for (int i = additional_queue_info; i < get_graph_length(graph); i++) {
		
		current = vertex_cover[i];

		if (current > -1 && !get_deleted_flag(graph, current) && get_position_flag(bipartition, current) == 2) {
				# ifdef _OPENMP
					write_to_bitmask(bitmask, get_graph_length(graph), locks, current, 1);
					elements_in_head_thread[omp_get_thread_num()]++;
				# endif
		}

	}

	# ifdef _OPENMP
		for (int i = 0; i < omp_get_num_procs(); i++) {
			elements_in_head += elements_in_head_thread[i];
		}
	# endif

  remove_vertices_through_bitmask(graph, bitmask, get_graph_length(graph));

  free(bitmask);
  free(vertex_cover);
	free(elements_in_head_thread);

	# ifdef _OPENMP
		free(locks);
	# endif

	return elements_in_head;

}
