#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <omp.h>
#include <math.h>

#include "../graph/graph.h"
#include "../graph/graph_builder.h"
#include "../graph/graph_functions.h"
#include "../graph/graph_functions_parallel.h"
#include "../matching/matching.h"
#include "../matching/matching_functions.h"
#include "../matching/matching_functions_parallel.h"
#include "../utilities/time_utilities.h"
#include "../vertex_list/vertex_list.h"
#include "../queue/queue.h"
#include "lp.h"

// Allows for usage of only a percentage of the cpus (use multiples of 2).
#define CPU_USAGE_FACTOR 1

extern int parameter;
extern int additional_graph_info;

int ***graph;
int ***bipartition;
int ***matching0;

int *min_vc;
int graph_trivial = 0;

int main(int argc, char **argv) {

  time_t t;
  time(&t);
  srand((unsigned) time(&t));

  # ifdef _OPENMP
		omp_set_num_threads(omp_get_num_procs() / CPU_USAGE_FACTOR);
	# endif

  graph = read_graph(stdin);

  //fprintf(stderr, "done reading the graph\n");

  if (count_vertices(graph) > 2 * parameter) {

    remove_isolated_vertices(graph);
    bipartition = create_graph(get_graph_length(graph) * 2);
    matching0 = create_graph(get_graph_length(bipartition));

    duplicate_graph_into_bipartition(graph, bipartition);

    //fprintf(stderr, "duplicated the graph\n");

    # ifdef _OPENMP
      push_relabel_parallel_preinit(bipartition, matching0);
    # else
      push_relabel_nonparallel(bipartition, matching0);
    # endif

    //fprintf(stderr, "maximum matching computed\n");

    min_vc = compute_min_vc_from_bipartition_and_matching(bipartition, matching0);

    //fprintf(stderr, "minvc computed\n");

    if (decide_ilp(graph, bipartition, min_vc) || parameter < 0) { // Check after solving ilp if parameter < 0.

      graph_trivial = -1;

    }
    else {

      if (parameter >= count_vertices(graph)) {

        graph_trivial = 1;

      }

      //fprintf(stderr, "decided\n");

    }

    free_graph(bipartition);
    free_graph(matching0);
    free(min_vc);

  }
  else if (count_vertices(graph) <= parameter) {

    graph_trivial = 1;

  }

  if (graph_trivial == -1) {

    output_no_instance();

  }
  else if (graph_trivial == 1) {

    output_yes_instance();

  }
  else {

    output_graph(graph);

  }

  free_graph(graph);

  return 0;

}

/*
  duplicate_graph_into_bipartition
  Duplicates the vertex set of a graph (vertex u into u1, u2) and for every edge
  u-v adds two edges u1-v2, v1-u2, resulting in a bipartite graph.
  Params:   graph - graph
            bipartition - an empty graph of only vertices,
              result will be stored here
*/
void duplicate_graph_into_bipartition(int ***graph, int ***bipartition) {

  int *vertex_degree = (int *)(calloc(get_graph_length(bipartition), sizeof(int)));
	int *edge_list_index = (int *)(calloc(get_graph_length(bipartition), sizeof(int)));

  int half_size = get_graph_length(graph);
  int other = -1;

  for (int this = 0; this < half_size; this++) {

    for (int j = additional_graph_info; j < graph[this][0][0]; j++) {

      other = graph[this][0][j];

      // Add every edge only once.
      if (other > this) { // This way, this > -1 and every edge is only added once.

        set_deleted_flag(bipartition, this, get_deleted_flag(graph, this));
        set_deleted_flag(bipartition, this + half_size, get_deleted_flag(graph, this));
        set_deleted_flag(bipartition, other, get_deleted_flag(graph, other));
        set_deleted_flag(bipartition, other + half_size, get_deleted_flag(graph, other));

        set_position_flag(bipartition, this, 1);
        set_position_flag(bipartition, other, 1);
        set_position_flag(bipartition, other + half_size, 2);
        set_position_flag(bipartition, this + half_size, 2);

        //add_undirected_edge(bipartition, this, other + half_size);
        //add_undirected_edge(bipartition, other, this + half_size);

        add_undirected_edge_fast(bipartition, vertex_degree, edge_list_index, this, other + half_size);
        add_undirected_edge_fast(bipartition, vertex_degree, edge_list_index, other, this + half_size);

      }

    }

  }

  free(vertex_degree);
  free(edge_list_index);

}

/*
  decide_ilp
  Decides if the ilp-kernelization in a graph resolves to
    unsolvable or otherwise issues an appropriate vertex
    removal.
  Params:   graph - a graph
            bipartition - a bipartition of graph, create with
              duplicate_graph_into_bipartition()
            min_vc - a queue of vertices that are a minimal vertex
              cover in bipartition
  Returns:  If unsolvable 1, otherwise 0.
*/
int decide_ilp(int ***graph, int ***bipartition, int *min_vc) {

  float ilp_solution_sum = 0;
  float *ilp_solution = (float *)(malloc(get_graph_length(bipartition) * sizeof(float)));
  int index = -1;
  int ret = -1;

  unsigned char *bitmask = (unsigned char *)(calloc((get_graph_length(graph) / 8 + 1), sizeof(unsigned char)));
  # ifdef _OPENMP
		omp_lock_t *locks = (omp_lock_t *)(malloc((get_graph_length(graph) / 8 + 1) * sizeof(omp_lock_t)));
		for (int i = 0; i < (get_graph_length(graph) / 8 + 1); i++) {
				omp_init_lock(&locks[i]);
		}
	# endif

  #pragma omp parallel
  {

    #pragma omp for
    for (int i = 0; i < get_graph_length(bipartition); i++) {

      ilp_solution[i] = 0;

    }

    #pragma omp for
    for (int i = get_queue_entries_start(min_vc); i < get_queue_length(min_vc); i++) {

      if (((index = min_vc[i])) > -1) {

        ilp_solution[index] += 0.5f;
        #pragma omp atomic
        ilp_solution_sum += 0.5f;

      }

    }

    /*
      Min-VC holds vertices of bipartite graph which is a duplication of
      the original graph.
    */
    #pragma omp for
    for (index = 0; index < get_graph_length(graph); index++) {

        ilp_solution[index] += ilp_solution[index + get_graph_length(graph)];

    }

  }

  if (trunc(ilp_solution_sum) > parameter) {

    ret = 1;

  }
  else {

    #pragma omp parallel for
    for (int i = 0; i < get_graph_length(graph); i++) {

      if (ilp_solution[i] < 0.5) {

        // Mark as 'to be deleted'.
        # ifdef _OPENMP
					write_to_bitmask(bitmask, get_graph_length(graph), locks, i, 1);
				# else
					bitmask[i / 8] |= 1 << (i % 8);
				# endif

      }
      else if (ilp_solution[i] > 0.5) {

        // Mark as 'to be deleted'.
        # ifdef _OPENMP
					write_to_bitmask(bitmask, get_graph_length(graph), locks, i, 1);
				# else
					bitmask[i / 8] |= 1 << (i % 8);
				# endif

        #pragma omp atomic
        parameter = parameter - 1;

      }

    }

    remove_vertices_through_bitmask(graph, bitmask, get_graph_length(graph));

    ret = 0;

  }

  free(ilp_solution);
  free(bitmask);

  return ret;

}
