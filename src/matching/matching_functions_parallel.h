void compute_maximal_matching_parallel(int ***, int ***);
void push_relabel_parallel_preinit(int ***, int ***);
void push_relabel_parallel_thread_init(int ***, int ***);
void push_relabel_parallel_thread_init_variable_loops(int ***, int ***, int);

void obtain_bipartition_from_matching_parallel(int ***, int ***, int ***);
