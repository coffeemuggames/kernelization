void compute_maximal_matching(int ***, int ***);

void obtain_bipartition_from_matching(int ***, int ***, int ***);
void compute_maximum_matching_in_bipartition(int ***, int ***);
void compute_flow_graph_hopcroft_karp(int ***, int ***, int ***);
void update_flow_graph_hopcroft_karp(int ***, int ***, int ***, int);

void push_relabel_nonparallel(int ***, int ***);
void push_relabel_nonparallel_hotstart(int ***, int ***, int *, int *, int);
void gap_relabeling(int ***, int ***, int *);

int * compute_min_vc_from_bipartition_and_matching(int ***, int ***);
void compute_flow_graph_min_vc(int ***, int ***, int ***);
