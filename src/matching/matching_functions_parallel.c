#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>
#include "../graph/graph.h"
#include "matching.h"
#include "matching_functions.h"
#include "matching_functions_parallel.h"
#include "../queue/queue.h"
#include "../vertex_list/vertex_list.h"

// Allows file global definition of work package size for each CPU.
#define WORK_PACKAGE_SIZE 128

extern int initial_edge_list_size;
extern int additional_graph_info;

/*
	compute_maximal_matching_parallel
	Computes a maximal matching in a graph in parallel. A maximal matching
		is a subset of edges of a graph so that no more edges could be
		added without violating the conditions for a matching.
	Params:		graph - a graph
						matching - a graph in which the matching will be embedded,
							consisting of all of the vertices but only those edges
							that are part of the matching (according to graph)
*/
void compute_maximal_matching_parallel(int ***graph, int ***matching) {

	// 1 = blue
	// 2 = red
	// 0 = dead

	int *color = (int *)(malloc(get_graph_length(graph) * sizeof(int)));
	int **proposed = (int **)(malloc(get_graph_length(graph) * sizeof(int *)));
	int proposal = 0;
	int is_active = 1;
	int i = -1;

	#pragma omp parallel for schedule(static, WORK_PACKAGE_SIZE)
	for (i = 0; i < get_graph_length(graph); i++) {

		color[i] = 1;
		proposed[i] = create_queue(4);

		if (get_deleted_flag(graph, i)) {

			color[i] = 0;

		}

	}

	while(is_active) {

		is_active = 0;

		// Choose colors randomly.

		#pragma omp parallel for schedule(static, WORK_PACKAGE_SIZE)
		for (i = 0; i < get_graph_length(graph); i++) {

			if (color[i] > 0) {

				color[i] = rand() % 2 + 1;
				is_active = 1;

			}

		}

		// Blue vertices propose to red vertices.

		#pragma omp parallel for schedule(static, WORK_PACKAGE_SIZE) private (proposal)
		for (i = 0; i < get_graph_length(graph); i++) {

			if (color[i] == 1) {

				proposal = get_random_red_neighbor(graph, i, color);

				if (proposal > -1) {

					push_queue(proposed[proposal], i);

				}
				else if (proposal == -2) {

					color[i] = 0;

				}

			}

		}

		// Red vertices choose any one of the proposals made.

		#pragma omp parallel for schedule(static, WORK_PACKAGE_SIZE) private (proposal)
		for (i = 0; i < get_graph_length(graph); i++) {

			if (color[i] == 2) {

				proposal = pop_queue(proposed[i]);
				if (proposal > -1) {

					add_undirected_edge(matching, i, proposal);
					color[i] = 0;
					color[proposal] = 0;

				}
				while((proposal = pop_queue(proposed[i])) != -1); // Empty queue.

			}

		}

	}

	free(color);
	free(proposed);

}

/*
	push_relabel_parallel_preinit
	Computes a maximum matching in a bipartition with the parallel push relabel
		algorithm. Initializations of queues for threads before any parallel work is
		done, therefore queues are not managed by OpenMP.
	Params:		bipartition - the graph
						maximum_matching - a graph of only the vertices,
							which is turned into a maximum matching
*/
void push_relabel_parallel_preinit(int ***bipartition, int ***maximum_matching) {

	int n = get_graph_length(bipartition) / 2;
	int elem = -1, cnt = 0, handled = 0;
	int solve_sequential = 0;
	int *psi = (int *)(malloc(get_graph_length(bipartition) * sizeof(int)));
	int *active = create_queue(get_graph_length(bipartition));
	int *active_compactified = (int *)(malloc(get_graph_length(bipartition) * sizeof(int)));

	omp_lock_t *locks = (omp_lock_t *)(malloc(get_graph_length(bipartition) * sizeof(omp_lock_t)));
	int num_threads = 1;

	# ifdef _OPENMP
		num_threads = omp_get_num_procs();
	# endif

	// Initialize queue for each thread.
	int **queues = (int **)(malloc(num_threads * sizeof(int *)));
	for (int i = 0; i < num_threads; i++) {

		queues[i] = create_queue(get_graph_length(bipartition));

	}


	for (int i = 0; i < get_graph_length(bipartition); i++) {

		# ifdef _OPENMP
			omp_init_lock(&locks[i]);
		# endif

		psi[i] = get_graph_length(bipartition) + 1;

		if (!get_deleted_flag(bipartition, i) && get_position_flag(bipartition, i) == 1) {

			psi[i] = 0;

			if (is_free_in_matching(bipartition, maximum_matching, i)) {
				push_queue(active, i);
			}

		}
		else if (!get_deleted_flag(bipartition, i) && get_position_flag(bipartition, i) == 2) {

			psi[i] = 1;

		}

	}

	int is_active = 1;
	int u = -1, v = -1;
	int i = 0, k = 0;
	int neighbor = -1;

	while(is_active) {

		is_active = 0;

		elem = -1;
    cnt = 0;
    while ((elem = pop_queue(active)) != -1) {

      active_compactified[cnt] = elem;
      cnt++;

    }

		#pragma omp parallel for private(u, v, neighbor, k, i)
		for (int it = 0; it < cnt; it++) {

      v = active_compactified[it];

			if (!has_edge(maximum_matching, v) && psi[v] < get_graph_length(bipartition)) {

				// Find u. ***
				u = -1;
				for (k = additional_graph_info; k < bipartition[v][0][0]; k++) {

					neighbor = bipartition[v][0][k];

					if (neighbor > -1 && !get_deleted_flag(bipartition, neighbor)) {

						if (u == -1 || psi[neighbor] < psi[u]) {

							u = neighbor;

						}

					}

				}
				// Find u. ***

				if (psi[u] < get_graph_length(bipartition)) {

					# ifdef _OPENMP
						omp_set_lock(&locks[u]);
					# endif

					#pragma omp atomic
					handled++;
					psi[v] = psi[u] + 1;

					if (has_edge(maximum_matching, u)) {

						# ifdef _OPENMP
							push_queue(queues[omp_get_thread_num()], maximum_matching[u][0][additional_graph_info]);
						# else
							push_queue(queues[0], maximum_matching[u][0][additional_graph_info]);
						# endif

						remove_undirected_edge(maximum_matching, u, maximum_matching[u][0][additional_graph_info]);
						is_active = 1;

					}

					add_undirected_edge(maximum_matching, v, u);
					psi[u] = psi[u] + 2;

					# ifdef _OPENMP
						omp_unset_lock(&locks[u]);
					# endif

				}

			}

		}

		// Join queues into active.
		// Join queues into active. Thread-safe because parallel region is over.
		elem = -1;
		for (i = 0; i < num_threads; i++) {

			while ((elem = pop_queue(queues[i])) != -1) {

				if (!has_edge(maximum_matching, elem)) {

					push_queue(active, elem);
					is_active = 1;

				}

			}

		}

		if (handled > n) {

			handled = 0;
			gap_relabeling(bipartition, maximum_matching, psi);

		}

		if (cnt < 10000 && cnt > 0) {

			solve_sequential = 1;
			break;

		}

	}

	for (int i = 0; i < num_threads; i++) {

		free(queues[i]);

	}
	free(queues);
	free(active_compactified);
	free(locks);

	if (solve_sequential) {

		push_relabel_nonparallel_hotstart(bipartition, maximum_matching, psi, active, n);

	}

	free(psi);
	free(active);

}

/*
	obtain_bipartition_from_matching_parallel
	Computes a bipartition of a graph in parallel, so that all the unmatched
	 	vertices are on one side, their neighbors on the other.The partition's
		edges are inserted into a given graph.
	Params:		graph - a graph
						matching - the matching in the graph
						bipartition - a graph in which the edges of the bipartition
							will be inserted
*/
void obtain_bipartition_from_matching_parallel(int ***graph, int ***matching, int ***bipartition) {

	int other = -1, i = -1;

	int *vertex_degree = (int *)(calloc(get_graph_length(graph), sizeof(int)));
	int *edge_list_index = (int *)(calloc(get_graph_length(graph), sizeof(int)));

	# ifdef _OPENMP
		omp_lock_t *locks = (omp_lock_t *)(malloc(get_graph_length(graph) * sizeof(omp_lock_t)));
		for (int i = 0; i < get_graph_length(graph); i++) omp_init_lock(&locks[i]);
	# endif

	#pragma omp parallel for private(i, other) schedule(static, WORK_PACKAGE_SIZE)
	for (int this = 0; this < get_graph_length(graph); this++) {

		if (!get_deleted_flag(graph, this) && is_free_in_matching(graph, matching, this)) {

			for (i = additional_graph_info; i < graph[this][0][0]; i++) {

				other = graph[this][0][i];

				if (other > -1) {

					// every edge is only inserted once

					if (!get_deleted_flag(graph, other) && is_matched(graph, matching, other)) {

						# ifdef _OPENMP
							if (this < other) {
									omp_set_lock(&locks[this]);
									omp_set_lock(&locks[other]);
							}
							else {
								omp_set_lock(&locks[other]);
								omp_set_lock(&locks[this]);
							}
						# endif

						if (!is_undirected_edge(bipartition, this, other)) {

							// setting a flag for the vertex for side telling
							set_position_flag(bipartition, this, 1);
							set_position_flag(bipartition, other, 2);

							add_undirected_edge_fast(bipartition, vertex_degree, edge_list_index, this, other);

						}

						# ifdef _OPENMP
							if (this < other) {
									omp_unset_lock(&locks[other]);
									omp_unset_lock(&locks[this]);
							}
							else {
								omp_unset_lock(&locks[this]);
								omp_unset_lock(&locks[other]);
							}
						# endif

					}

				}

			}

		}

	}

	free(vertex_degree);
	free(edge_list_index);

	# ifdef _OPENMP
		free(locks);
	# endif

}
