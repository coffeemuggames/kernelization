#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "matching_functions.h"
#include "matching.h"
#include "../graph/graph.h"
#include "../graph/graph_builder.h"
#include "../graph/graph_functions.h"
#include "../queue/queue.h"
#include "../vertex_list/vertex_list.h"

extern int initial_edge_list_size;
extern int additional_graph_info;

/*
	compute_maximal_matching
	Computes a maximal matching in a graph, that is a subset of
		edges of a graph so that no more edges could be added without
		violating the conditions for a matching.
	Params:		graph - a graph
						matching - a graph in which the matching will be embedded,
							consisting of all of the vertices but only those edges
							that are part of the matching (according to graph)
	Returns:	matched - an array with each entry indicating whether or
							not that vertex has been matched
*/
void compute_maximal_matching(int ***graph, int ***matching) {

	int *marked = (int *)(malloc(get_graph_length(graph) * sizeof(int)));
	int other = -1;

	for (int i = 0; i < get_graph_length(graph); i++) {

		marked[i] = 0;

	}

	for (int this = 0; this < get_graph_length(graph); this++) {

		if (!get_deleted_flag(graph, this) && !marked[this]) {

			for (int i = additional_graph_info; i < graph[this][0][0]; i++) {

				other = graph[this][0][i];

				if (other > -1) {

					if (!get_deleted_flag(graph, other) && !marked[other]) {

						add_undirected_edge(matching, this, other);
						marked[this] = 1;
						marked[other] = 1;
						break;

					}

				}

			}

		}

	}

	free(marked);

}

/*
	push_relabel_nonparallel
	Computes a maximum matching in a bipartition with the sequential push relabel
		algorithm.
	Params:		bipartition - the graph
						maximum_matching - a graph of only the vertices,
							which is turned into a maximum matching
*/
void push_relabel_nonparallel(int ***bipartition, int ***maximum_matching) {

	int n = get_graph_length(bipartition) / 2;
	int *psi = (int *)(malloc(get_graph_length(bipartition) * sizeof(int)));
	int *active = create_queue(get_graph_length(bipartition));

	for (int i = 0; i < get_graph_length(bipartition); i++) {

		psi[i] = get_graph_length(bipartition);

		if (!get_deleted_flag(bipartition, i) && get_position_flag(bipartition, i) == 1) {

			psi[i] = 0;

			if (is_free_in_matching(bipartition, maximum_matching, i)) {

				push_queue(active, i);

			}

		}
		else if (!get_deleted_flag(bipartition, i) && get_position_flag(bipartition, i) == 2) {

			psi[i] = 1;

		}

	}

	push_relabel_nonparallel_hotstart(bipartition, maximum_matching, psi, active, n);

	free(psi);
	free(active);

}

/*
	push_relabel_nonparallel_hotstart
	Computes a maximum matching in a bipartition with the sequential push relabel
		algorithm. This function is called after various initializations have already
		happened. Usually, this would be after the parallel version of push relabel
		has done some work on the instance.
	Params:		bipartition - the graph
						maximum_matching - a graph of only the vertices,
							which is turned into a maximum matching
						psi - a labeling of the vertices in graph
						active - the active queue
						n - the number of vertices on the right side of the bipartition
*/
void push_relabel_nonparallel_hotstart(int ***bipartition, int ***maximum_matching, int *psi, int *active, int n) {

	int handled = 0;
	int k = 0;
	int u = -1, v = -1;
	int neighbor = -1;

	while ((v = pop_queue(active)) != -1) {

		if (!has_edge(maximum_matching, v)) {

			// Find u. ***
			u = -1;
			for (k = additional_graph_info; k < bipartition[v][0][0]; k++) {

				neighbor = bipartition[v][0][k];

				if (neighbor > -1 && !get_deleted_flag(bipartition, neighbor)) {

					if (u == -1 || psi[neighbor] < psi[u]) {

						u = neighbor;

						if (psi[u] == psi[v] - 1) {

							break;

						}

					}

				}

			}
			// Find u. ***

			if (psi[u] < get_graph_length(bipartition)) {

				psi[v] = psi[u] + 1;
				handled++;

				if (has_edge(maximum_matching, u)) {

					push_queue(active, maximum_matching[u][0][additional_graph_info]);
					remove_undirected_edge(maximum_matching, u, maximum_matching[u][0][additional_graph_info]);

				}

				add_undirected_edge(maximum_matching, v, u);
				psi[u] = psi[u] + 2;

			}

		}

		if (handled > n) {

			handled = 0;
			gap_relabeling(bipartition, maximum_matching, psi);

		}

	}

}

/*
	gap_relabeling
	Performs gap relabeling on a labeling of vertices from push relabeling
		to delete vertices that are no longer interesting to the maximum
		matching. This functions uses a bottom up approach.
	Params:		bipartition - a bipartition
						matching - a matching in bipartition
						psi - a labeling of vertices in bipartition with respect to matching
*/
void gap_relabeling(int ***bipartition, int ***matching, int *psi) {

	int *checked = (int *)(malloc((get_graph_length(bipartition) + 2) * sizeof(int)));

	for (int i = 0; i < get_graph_length(bipartition) + 2; i++) {

		checked[i] = 0;

	}

	for (int i = 0; i < get_graph_length(bipartition); i++) {

		checked[psi[i]] = 1;

	}

	int gap = -1;
	int gap_valid = 0;

	for (int i = 1; i <= get_graph_length(bipartition) - 1; i++) {

		if (checked[i] == 0) {

			gap = i;

		}
		else {

			if (gap != -1) {

				gap_valid = 1;
				break;

			}

		}

	}

	int cnt = 0;

	if (gap_valid) {

		for (int i = 0; i < get_graph_length(bipartition); i++) {

			if (psi[i] > gap) {

				psi[i] = get_graph_length(bipartition) + 1;
				cnt++;

			}

		}

	}

	free(checked);

}

/*
	compute_maximum_matching_in_bipartition
	Computes a maximum matching in a bipartition.
	Params:		bipartition - the graph
						maximal_matching - a maximal matching in the graph, which is turned
							into a maximum matching
*/
void compute_maximum_matching_in_bipartition(int ***bipartition, int ***maximal_matching) {

	// Plus two vertices: first one is start, other one is target.
	int ***flow_graph = create_graph(get_graph_length(bipartition) + 2);

	// Prepare graph with start, target and other directed edges.
	compute_flow_graph_hopcroft_karp(bipartition, maximal_matching,
		flow_graph);

	int *path = (int *)(malloc(get_graph_length(bipartition) * sizeof(int)));

	while (get_path_in_directed_graph(flow_graph, get_graph_length(bipartition) + 2, get_graph_length(bipartition),
		get_graph_length(bipartition) + 1, path)) {

		for (int i = 2; i < path[0] - 1; i++) {

			if (is_undirected_edge(maximal_matching, path[i], path[i + 1])) {

				remove_undirected_edge(maximal_matching, path[i], path[i + 1]);

			}
			else {

				add_undirected_edge(maximal_matching, path[i], path[i + 1]);

			}

			flip_directed_edge(flow_graph, path[i], path[i+1]);
			update_flow_graph_hopcroft_karp(flow_graph, bipartition, maximal_matching, path[i]);
			update_flow_graph_hopcroft_karp(flow_graph, bipartition, maximal_matching, path[i+1]);

		}

	}

	free_graph(flow_graph);
	free(path);

}

/*
	compute_flow_graph_hopcroft_karp
	Computes a flow graph for hopcroft-karp algorithm.
	Params:		bipartition - a bipartite graph
						maximal_matching - a maximal matching in the bipartiton
						flow_graph	- a graph in which the directed edges
							will be inserted
*/
void compute_flow_graph_hopcroft_karp(int ***bipartition, int ***maximal_matching, int ***flow_graph) {

	int other = -1;

	for (int i = 0; i < get_graph_length(bipartition); i++) {

		if (!get_deleted_flag(bipartition, i)) {

			if (get_position_flag(bipartition, i) == 1
				&& is_free_in_matching(bipartition, maximal_matching, i)) {

				add_directed_edge(flow_graph, get_graph_length(bipartition), i);

			}
			else if (get_position_flag(bipartition, i) == 2 &&
						is_free_in_matching(bipartition, maximal_matching, i)) {

				add_directed_edge(flow_graph, i, get_graph_length(bipartition) + 1);

			}

			for (int j = additional_graph_info; j < bipartition[i][0][0]; j++) {

				other = bipartition[i][0][j];

				if (other > -1) {

					if (!get_deleted_flag(bipartition, other) &&
						((get_position_flag(bipartition, i) == 2 && is_undirected_edge(maximal_matching, i , other))
						|| (get_position_flag(bipartition, i) == 1 && !is_undirected_edge(maximal_matching, i , other)
									&& is_undirected_edge(bipartition, i, other)))) {

						add_directed_edge(flow_graph, i, other);

					}

				}

			}

		}
		else {

			set_deleted_flag(flow_graph, i, 1);

		}

	}

}

/*
	obtain_bipartition_from_matching
	Computes a bipartition of a graph, so that all the unmatched vertices
		are on one side, their neighbors on the other.The partition's
		edges are inserted into a given graph.
	Params:		graph - a graph
						matching - the matching in the graph
						bipartition - a graph in which the edges of the bipartition
							will be inserted
*/
void obtain_bipartition_from_matching(int ***graph, int ***matching, int ***bipartition) {

	int other = -1, i = -1;
	int *vertex_degree = (int *)(calloc(get_graph_length(graph), sizeof(int)));
	int *edge_list_index = (int *)(calloc(get_graph_length(graph), sizeof(int)));

	for (int this = 0; this < get_graph_length(graph); this++) {

		if (!get_deleted_flag(graph, this) && is_free_in_matching(graph, matching, this)) {

			for (i = additional_graph_info; i < graph[this][0][0]; i++) {

				other = graph[this][0][i];

				if (other > -1) {

					// every edge is only inserted once

					if (!get_deleted_flag(graph, other) && !is_undirected_edge(bipartition, this, other)
						&& is_matched(graph, matching, other)) {

						// setting a flag for the vertex for side telling
						set_position_flag(bipartition, this, 1);
						set_position_flag(bipartition, other, 2);

						add_undirected_edge_fast(bipartition, vertex_degree, edge_list_index, this, other);

					}

				}

			}

		}

	}

	free(edge_list_index);
	free(vertex_degree);

}

/*
	update_flow_graph_hopcroft_karp
	Updates a flow graph induced by a matching in a bipartition for hopcroft-karp.
	Params:		flow_graph - a flow graph obtained by compute_flow_graph
						bipartition - a bipartition which flow_graph uses
						maximal_matching - a maximal matching in bipartition
						path - a augmenting path in the flow_graph with respect to
							maximal_matching
						vertex - the current vertex which shall be updated
*/
void update_flow_graph_hopcroft_karp(int ***flow_graph, int ***bipartition, int ***maximal_matching, int vertex) {

	remove_directed_edge(flow_graph, get_graph_length(bipartition), vertex);
	remove_directed_edge(flow_graph, vertex, get_graph_length(bipartition) + 1);

	if (is_free_in_matching(bipartition, maximal_matching, vertex)) {

		if (get_position_flag(bipartition, vertex) == 1) {

			add_directed_edge(flow_graph, get_graph_length(bipartition), vertex);

		}
		else if (get_position_flag(bipartition, vertex) == 2) {

			add_directed_edge(flow_graph, vertex, get_graph_length(bipartition) + 1);

		}

	}

}

/*
	compute_min_vc_from_bipartition_and_matching
	Computes a minimal size vertex cover in a bipartite graph
		with a maximum matching.
	Params:		bipartition - a bipartite graph
						matching - a maximum matching in bipartition
	Returns:	vertex_cover - a queue, holding vertices which constitute the vertex cover
*/
int * compute_min_vc_from_bipartition_and_matching(int ***bipartition, int ***matching) {

	int ***flow_graph = create_graph(get_graph_length(bipartition));
	int *queue = create_queue(get_graph_length(bipartition));
	int *visited = (int *)(malloc(get_graph_length(bipartition) * sizeof(int)));
	int elem = -1, neighbor = -1;

	int *Z = (int *)(malloc(get_graph_length(bipartition) * sizeof(int)));
	int *vertex_cover = create_queue(get_graph_length(bipartition));

	compute_flow_graph_min_vc(bipartition, matching, flow_graph);

	for (int i = 0; i < get_graph_length(bipartition); i++) {

		visited[i] = 0;
		Z[i] = 0;

		if (!get_deleted_flag(bipartition, i) && get_position_flag(bipartition, i) == 1
				&& is_free_in_matching(bipartition, matching, i)) {

				push_queue(queue, i);
				Z[i] = 1;

		}

	}

	while ((elem = pop_queue(queue)) != -1) {

		for (int i = additional_graph_info; i < flow_graph[elem][0][0]; i++) {

			neighbor = flow_graph[elem][0][i];

			if (neighbor > -1 && !get_deleted_flag(flow_graph, neighbor) && !visited[neighbor]) {

				push_queue(queue, neighbor);
				Z[neighbor] = 1;
				visited[neighbor] = 1;

			}

		}

	}

	for (int i = 0; i < get_graph_length(bipartition); i++) {

		if (!get_deleted_flag(bipartition, i) &&
				((get_position_flag(bipartition, i) == 1 && !Z[i])
					|| (get_position_flag(bipartition, i) == 2 && Z[i]))) {

			push_queue(vertex_cover, i);

		}

	}

	free_graph(flow_graph);
	free(queue);
	free(visited);
	free(Z);

	return vertex_cover;

}

/*
	compute_flow_graph_min_vc
	Computes a flow graph for computing a minimum vertex cover in a bipartition.
	Params:		bipartition - a bipartite graph
						maximal_matching - a maximal matching in the bipartition
						flow_graph	- a graph in which the directed edges
							will be inserted
*/
void compute_flow_graph_min_vc(int ***bipartition, int ***maximal_matching, int ***flow_graph) {

	int *vertex_degree = (int *)(calloc(get_graph_length(flow_graph), sizeof(int)));
	int *edge_list_index = (int *)(calloc(get_graph_length(flow_graph), sizeof(int)));
	int *matched_to = (int *)(malloc(get_graph_length(bipartition) * sizeof(int)));

	int other = -1;

	for (int i = 0; i < get_graph_length(maximal_matching); i++) matched_to[i] = -1;

	for (int i = 0; i < get_graph_length(maximal_matching); i++) {

		if (!get_deleted_flag(bipartition, i)) {

			for (int j = additional_graph_info; j < maximal_matching[i][0][0]; j++) {

				other = maximal_matching[i][0][j];

				if (other > -1 && !get_deleted_flag(bipartition, other)) {

					matched_to[i] = other;
					matched_to[other] = i;

				}

			}

		}

	}

	for (int i = 0; i < get_graph_length(bipartition); i++) {

		if (!get_deleted_flag(bipartition, i)) {

			for (int j = additional_graph_info; j < bipartition[i][0][0]; j++) {

				other = bipartition[i][0][j];

				if (other > -1 && !get_deleted_flag(bipartition, other)) {

					if ((get_position_flag(bipartition, i) == 2 && matched_to[i] == other)
						|| (get_position_flag(bipartition, i) == 1 && matched_to[i] != other)) {

						add_directed_edge_fast(flow_graph, vertex_degree, edge_list_index, i, other);

					}

				}

			}

		}
		else {

			set_deleted_flag(flow_graph, i, 1);

		}

	}

	free(matched_to);
	free(vertex_degree);
	free(edge_list_index);

}
