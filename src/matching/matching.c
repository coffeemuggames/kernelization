#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "matching.h"
#include "matching_functions.h"
#include "../graph/graph.h"
#include "../queue/queue.h"
#include "../vertex_list/vertex_list.h"

int initial_edge_list_size = 8;

/*
	is_free_in_matching
	Indicates if a vertex is free in respect to a matching (it is
		unmatched) in a graph
	Params:		graph - a graph
						matching - a matching in the graph
						vertex - a vertex in the graph
	Returns:	free - is 0 if the vertex is matched (or deleted)
							unequal to zero otherwise
*/
int is_free_in_matching(int ***graph, int ***matching, int vertex) {

	return !get_deleted_flag(graph, vertex) && !has_edge(matching, vertex);

}

/*
	is_matched
	Indicates if a vertex is matched in respect to a matching
		in a graph.
	Params:		graph - a graph
						matching - a matching in the graph
						vertex - a vertex in the graph
	Returns:	matched - is 0 if the vertex is deleted or unmatched
							unequal to zero otherwise
*/
int is_matched(int ***graph, int ***matching, int vertex) {

	return !get_deleted_flag(graph, vertex) && has_edge(matching, vertex);

}

/*
	get_random_red_neighbor
	Gets a random neighbor colored red of a vertex.
	Params:		graph - a graph
						vertex - a vertex in the graph
	Returns:	neighbor - a random neighbor of vertex in graph that is of red color,
							-1 if no such neighbor could be found, -2 if all neighbors are
							dead.
*/
int get_random_red_neighbor(int ***graph, int vertex, int *color) {

	int ret = -1;
	int neighbor = 0;
	int iterator = 0;
	int len = 0;
	int alive = 0;
	int *vertex_list = create_vertex_list(graph[vertex][0][0]);

	while ((neighbor = get_next_neighbor(graph, vertex, &iterator)) != -1) {

		if (color[neighbor] == 2) {

			add_vertex_to_list(vertex_list, neighbor);
			len++;

		}
		else if (color[neighbor] > 0) {

			alive++;

		}

	}

	if (len > 0) {

		ret = get_vertex_from_list_by_index(vertex_list, rand() % len);
		free(vertex_list);
		return ret;

	}
	else if (alive) {

		free(vertex_list);
		return -1;

	}
	else {

		free(vertex_list);
		return -2;

	}

}
