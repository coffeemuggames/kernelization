# Efficient Parallel Kernel Algorithms for the Vertex Cover Problem

Includes optimized parallel and sequential versions of buss kernelization, crown decomposition, 
kernelization through linear programming and a solver for the NP-complete vertex cover problem.

Code for a bachelor thesis. Includes the following.

	/build			builds of source
	/doc			documentation
	/src			source files
	/stats		tools for retrieval of statistical data	

# Instructions

To build from source run 
```bash
	./make.sh 
```
in /build. Read documentation of programs in /doc for information
on further usage.

# Dependencies

To build, gcc compiler needs to be installed (https://gcc.gnu.org/).
If you want to exploit parallelism, make sure that GOMP is included in your gcc compiler 
(https://gcc.gnu.org/projects/gomp/).
