# First input parameter is parameter k, following up
# are an arbitrary amount of files name for graphs.

TIMEFORMAT=%R
args=("$@")
counter=0
mink=0
maxk=10
loops=1
algo=0
cmd=""
walk_n="false"

for ((i=0; i<${#args[@]}; i++)); do

	if [ "${args[i]}" = "-help" ]; then

		echo "get_stats"
		echo "Retrieves statistical data for kernelization with buss, crown decomposition and linear programming."
		echo "For documentation see ./doc/"
		exit 1

	elif [ "${args[i]}" = "-walkn" ]; then

		walk_n="true"

	elif [ "${args[i]}" = "-maxk" ]; then

		maxk=${args[i+1]}
		i=$(($i + 1))

	elif [ "${args[i]}" = "-mink" ]; then

		mink=${args[i+1]}
		i=$(($i + 1))

	elif [ "${args[i]}" = "-loops" ]; then

		loops=${args[i+1]}
		i=$(($i + 1))

	elif [ "${args[i]}" = "-algo" ]; then

		algo=${args[i+1]}
		arr_in=(${algo//./ })
		i=$(($i + 1))

		for ((j=0; j<${#arr_in[@]}; j++)); do

			if [ "${arr_in[j]}" = "bn" ]; then

				cmd="$cmd ./../code/build/buss/buss_nonparallel"

			elif [ "${arr_in[j]}" = "bp" ]; then

				cmd="$cmd ./../code/build/buss/buss_parallel"

			elif [ "${arr_in[j]}" = "cn" ]; then

				cmd="$cmd ./../code/build/crown/crown_nonparallel"

			elif [ "${arr_in[j]}" = "cp" ]; then

				cmd="$cmd ./../code/build/crown/crown_parallel"

			elif [ "${arr_in[j]}" = "lpn" ]; then

				cmd="$cmd ./../code/build/lp/lp_nonparallel"

			elif [ "${arr_in[j]}" = "lpp" ]; then

				cmd="$cmd ./../code/build/lp/lp_parallel"
				
			elif [ "${arr_in[j]}" = "vsn" ]; then

				cmd="$cmd ./../code/build/vc_solver/vc_solver_nonparallel"
		
			elif [ "${arr_in[j]}" = "vsp" ]; then
		
				cmd="$cmd ./../code/build/vc_solver/vc_solver_parallel"

			fi

			if [ $j -lt $((${#arr_in[@]} - 1)) ]; then

				cmd="$cmd |"

			fi


		done

	else

		files[counter]=${args[i]}
		counter=$(($counter + 1))

	fi

done

if [ ${#files[@]} = 0 ]; then

	echo "You need to provide with at least one graph."
	echo "For documentation see ./doc/"
	exit

fi

if [ "$walk_n" = "true" ]; then
	
	echo "n,k,time,vertexcount,vertexcountxn,edgecount,vertexdeletionratio,name"

elif [ "$walk_n" = "false" ]; then

	echo "k,time,vertexcount,vertexcountxn,edgecount,vertexdeletionratio"
	
fi

echo $cmd

for ((k=mink; k<=$maxk; k++)); do

	time=0
	start_vertex_count=0
	end_vertex_count=0
	ratio=0
	vertex_count=0
	edge_count=0
	solved_graph_count=0



	for ((i=0; i<${#files[@]}; i++)); do

		# save vertex count of input graph

		in=$(cat ${files[i]} | grep "^p")
		arr_in=(${in// / })

		start_vertex_count=${arr_in[2]}

		# save vertex count of kernel

		in=$( cat ${files[i]} | ./../code/build/graph/graph_append $k $loops | ( eval $cmd ) | grep -e "^p" -e "^c time" )
		arr_in=(${in// / })

		end_vertex_count=${arr_in[2]}
		end_edge_count=${arr_in[3]}
		vertex_count=$(($vertex_count + $end_vertex_count))
		edge_count=$(($edge_count + $end_edge_count))

		# read time without i/o

		cur_time=0
		if [ "${arr_in[6]}" = "computation" ]; then
			cur_time=${arr_in[7]}
		fi
		time=$(echo "scale=4;$time + $cur_time" | bc)


		if [ $end_vertex_count -le 2 ]; then

			solved_graph_count=$(($solved_graph_count + 1))

		fi

		# calculate reduction ratio

		ratio_add=$(echo "scale=4;$end_vertex_count / $start_vertex_count" | bc)
		ratio_add=$(echo "scale=4;( 1 - $ratio_add)" | bc)
		ratio=$(echo "scale=4;$ratio + $ratio_add" | bc)
		
		if [ "$walk_n" = "true" ]; then		

			echo "$start_vertex_count,$k,$cur_time,$end_vertex_count,-,$end_edge_count,$ratio_add,${files[i]}"

		fi

	done

	# calculate average values

	if [ "$walk_n" = "false" ]; then		

		time=$(echo "scale=4;$time / ${#files[@]}" | bc)
		ratio=$(echo "scale=4;$ratio / ${#files[@]}" | bc)

		vertex_count_xn=$(($vertex_count * $solved_graph_count))
		vertex_count_xn=$(($vertex_count_xn / ${#files[@]}))

		echo "$k,$time,$(($vertex_count / ${#files[@]})),$vertex_count_xn,$(($edge_count / ${#files[@]})),$ratio"

	fi

done
